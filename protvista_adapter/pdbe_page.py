from multiprocessing import context
from protvista_adapter.common_params import common_color_codes, funpdbe_resource_dict, funpdbe_resource_type_dict, domains_color_palette, domains_dict, quality_details
from protvista_adapter.common_params import color_protvista_predicted_annotations, color_protvista_curated_annotations, color_residue_conflicts, protvista_allowed_shapes
from protvista_adapter.util_common import assign_residue_colors_for_annotations, group_residues, split_to_fragments, create_resource_dict
from protvista_adapter.util_common import get_variation_color, get_source_type, format_variation_tooltip, get_variation_filter_keywords

def convert_uniprot_mapping(generic_response):
    """
        Summary:
            Converts generic uniprot mapping response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic uniprot mapping response
        Returns:
            A protvista specific uniprot mapping response object 
    """
    
    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")

    specific_response = {}
    
    for entry_id in generic_response.keys():
        entry_data = generic_response[entry_id]
        
        specific_response[entry_id] = {
            "sequence": entry_data["sequence"],
            "length": entry_data["length"],
            "tracks": [{
                "labelType": "text",
                "label": "UniProt",
                "data": []
            }]
        }

        for accession in entry_data["data"]:
            accession_fragment = {
                "color": "rgb(65,105,225)",
                "type": "UniProt",
                "labelType": "text",
                "tooltipContent": "UniProt {}".format(accession["accession"]),
                "accession": accession["accession"],
                "label": accession["accession"],
                "labelTooltip": "Mapping to UniProt {}".format(accession["name"]),
                "bestChainId": accession["additionalData"]["bestChainId"],
                "entityId": accession["additionalData"]["entityId"],
                "locations": [{
                    "fragments": []
                }]
            }
            for residues in accession["residues"]:
                residue_fragment = {
                    "start": residues["startIndex"],
                    "end": residues["endIndex"],
                    "unp_start": residues["unpStartIndex"],
                    "unp_end": residues["unpEndIndex"],
                    "tooltipContent": "Type: Mapping to UniProt<br>Range: {}{} - {}{} (Chain {})<br>Source: <a href='https://www.uniprot.org/uniprot/{}'>UniProt {}</a>".format(
                        residues["startCode"], residues["unpStartIndex"], residues["endCode"], residues["unpEndIndex"], accession["additionalData"]["bestChainId"], accession["accession"],
                        accession["accession"]
                    )
                }
                # check for mutation and modification
                if residues.get("mutation") is not None and residues.get("mutation") == True:
                    residue_fragment["color"] = color_residue_conflicts
                    mutation_type = residues.get("mutationType")
                    residue_fragment["tooltipContent"] += "<br>Conflict: {} --> {} ({})".format(residues["startCode"], residues["pdbCode"], mutation_type)
                elif residues.get("modification") is not None and residues.get("modification") == True:
                    residue_fragment["color"] = color_residue_conflicts
                    residue_fragment["tooltipContent"] += "<br>Modified residue: {}".format(residues["pdbCode"])

                accession_fragment["locations"][0]["fragments"].append(residue_fragment)

            specific_response[entry_id]["tracks"][0]["data"].append(accession_fragment)

    return specific_response
    

def convert_secondary_structures(generic_response):
    """
        Summary:
            Converts generic secondary structures response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic secondary structures response
        Returns:
            A protvista specific secondary structures response object 
    """
    
    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")

    specific_response = {}
    
    for entry_id in generic_response.keys():
        entry_data = generic_response[entry_id]
        master_dict = {}
        sec_structure_fragments = []
        idp_fragments = []
        topology_fragments = []
        efold_fragments = []

        specific_response[entry_id] = {
            "sequence": entry_data["sequence"],
            "length": entry_data["length"],
            "tracks": [],
            "legends": {
                "alignment": "right",
                "data": {
                    "Secondary structure": []
                }
            }
        }

        for sec_structure in entry_data["data"]:
            dict_key = (sec_structure["accession"], sec_structure["accession"])
            sec_structure_name = sec_structure["name"]

            # PDBE-4659: Assign color for WEBnma residues
            if sec_structure["dataType"] == "WEBnma":
                sec_structure["residues"] = assign_residue_colors_for_annotations(sec_structure["residues"], None)
            elif sec_structure["dataType"] in ["dynamine", "efoldmine"]:
                sec_structure["residues"] = assign_residue_colors_for_annotations(sec_structure["residues"], None, inverted=True)
            elif sec_structure["dataType"] == "KinCore":
                sec_structure["residues"] = group_residues(sec_structure["residues"], "additionalData.groupLabel")
            
            for residue in sec_structure["residues"]:
                if not master_dict.get(dict_key):
                    master_dict[dict_key] = {
                        "color": common_color_codes[sec_structure["accession"]],
                        "type": sec_structure["dataType"],
                        "labelType": "text",
                        "tooltipContent": sec_structure["name"],
                        "accession": sec_structure_name,
                        "label": sec_structure_name,
                        "labelTooltip": "Residues forming {}".format(sec_structure["accession"]),
                        "bestChainId": sec_structure["additionalData"]["bestChainId"],
                        "entityId": sec_structure["additionalData"]["entityId"],
                        "locations": [{
                            "fragments": []
                        }]
                    }

                residue_fragment = {
                    "start": residue["startIndex"],
                    "end": residue["endIndex"],
                    "tooltipContent": "Type: {}<br>Range: {}{} - {}{} (Chain {})".format(sec_structure["dataType"], residue["startCode"], residue["startIndex"],
                    residue["endCode"], residue["endIndex"], sec_structure["additionalData"]["bestChainId"])
                }
                if sec_structure["dataType"] == "MobiDB":
                    residue_fragment.update({
                        "tooltipContent": "Type: Consensus Disorder Prediction<br>Range: {}{} - {}{}<br>Source:<a href=\"http://mobidb.bio.unipd.it/\" target=\"_blank\">MobiDB</a>".format(
                            residue["startCode"], residue["startIndex"], residue["endCode"], residue["endIndex"]
                        )
                    })
                elif sec_structure["dataType"] == "KnotProt":
                    residue_fragment.update({
                        "tooltipContent": "Site id: {}<br>Type: Knot annotations<br>Label: {}<br>Range: {}{} - {}{}<br>Confidence level: {}".format(
                            residue["additionalData"]["ordinalId"], residue["additionalData"]["groupLabel"], residue["startCode"], residue["startIndex"],
                            residue["endCode"], residue["endIndex"], residue["additionalData"]["confidenceLevel"]
                        )
                    })
                elif sec_structure["dataType"] == "WEBnma":
                    residue_fragment.update({
                        "tooltipContent": "Site id: {}<br>Type: Flexibility prediction<br>Label: {}<br>PDB residue: {}{}<br>Source: <a href=\"http://apps.cbu.uib.no/webnma3\" target=\"_blank\">WEBnma</a><br>Raw score: {}".format(
                            residue["additionalData"]["ordinalId"], residue["additionalData"]["groupLabel"], residue["startCode"], residue["startIndex"],
                            residue["additionalData"]["rawScore"]
                        )
                    })
                    master_dict[dict_key]["type"] = "PDB range"
                elif sec_structure["dataType"] == "KinCore":
                    residue_fragment.update({
                        "tooltipContent": (
                            f"Type: KinCore classification<br>"
                            f'PDB Residue: {residue["endCode"]}{residue["endIndex"]}<br>'
                            f'Label: {residue["additionalData"]["groupLabel"]}<br>'
                            f'Chain identifier: {sec_structure["additionalData"]["bestChainId"]}<br>'
                            f'Source: <a target=\"_blank\" href=\"http://dunbrack.fccc.edu/kincore/PDB/{entry_id}\">KinCore</a><br>'
                            f'Confidence level: {residue["additionalData"]["confidenceLevel"]}<br>'
                        )
                    })
                elif sec_structure["dataType"] == "dynamine":
                    residue_fragment.update({
                        "tooltipContent": "Site id: {}<br>Type: Flexibility prediction<br>Label: {}<br>PDB residue: {}{}<br>Source: <a href=\"http://dynamine.ibsquare.be\" target=\"_blank\">DynaMine</a><br>Raw score: {}".format(
                            residue["additionalData"]["ordinalId"], residue["additionalData"]["groupLabel"], residue["startCode"], residue["startIndex"],
                            residue["additionalData"]["rawScore"]
                        )
                    })
                elif sec_structure["dataType"] == "efoldmine":
                    residue_fragment.update({
                        "tooltipContent": "Site id: {}<br>Type: Early folding residue prediction<br>Label: {}<br>PDB residue: {}{}<br>Source: <a href=\"http://dynamine.ibsquare.be\" target=\"_blank\">DynaMine</a><br>Raw score: {}".format(
                            residue["additionalData"]["ordinalId"], residue["additionalData"]["groupLabel"], residue["startCode"], residue["startIndex"],
                            residue["additionalData"]["rawScore"]
                        )
                    })
                
                # add color to residue fragment, if assigned
                if residue.get("color"):
                    residue_fragment.update({
                        "color": residue["color"]
                    })
                
                master_dict[dict_key]["locations"][0]["fragments"].append(residue_fragment)
        
        for (sec_structure_accession, sec_structure_name), values in master_dict.items():
            
            if sec_structure_accession == "MobiDB":
                values.update({
                    "labelColor": "rgb(211,211,211)",
                    "labelTooltip": "Consensus disorder prediction"
                })
                idp_fragments.append(values)
            elif sec_structure_accession in ["KnotProt", "KinCore"]:
                values.update({
                    "labelTooltip": f"Topology annotations from {sec_structure_accession}"
                })
                topology_fragments.append(values)
            elif sec_structure_accession in ["WEBnma", "dynamine"]:
                values.update({
                    "labelColor": "rgb(211,211,211)",
                    "labelTooltip": "Flexibility predictions"
                })
                if sec_structure_accession == "dynamine":
                    values.update({"label": "DynaMine"})
                idp_fragments.append(values)
            elif sec_structure_accession == "efoldmine":
                values.update({
                    "labelColor": "rgb(211,211,211)",
                    "labelTooltip": "Early folding residue predictions",
                    "label": "EFoldMine"
                })
                efold_fragments.append(values)
            else:
                # helix or strand
                sec_structure_fragments.append(values)

            # add legends
            if sec_structure_accession not in [x["text"] for x in specific_response[entry_id]["legends"]["data"]["Secondary structure"]]:
                sec_structure_accession_label = sec_structure_accession
                if sec_structure_accession == "dynamine":
                    sec_structure_accession_label = "DynaMine"
                elif sec_structure_accession == "efoldmine":
                    sec_structure_accession_label = "EFoldMine"

                specific_response[entry_id]["legends"]["data"]["Secondary structure"].append({
                    "color": common_color_codes[sec_structure_accession],
                    "text": sec_structure_accession_label
                })

        if sec_structure_fragments:
            specific_response[entry_id]["tracks"].append({
                "labelType": "text",
                "label": "Secondary structure",
                "data": sec_structure_fragments,
                "overlapping": "true"
            })
        if idp_fragments:
            specific_response[entry_id]["tracks"].append({
                "label": "Flexibility predictions",
                "labelType": "text",
                "data": idp_fragments,
                "labelColor": "rgb(128,128,128)"
            })
        if topology_fragments:
            specific_response[entry_id]["tracks"].append({
                "label": "Topology annotations",
                "labelType": "text",
                "data": topology_fragments,
            })
        if efold_fragments:
            specific_response[entry_id]["tracks"].append({
                "label": "Early folding residue predictions",
                "labelType": "text",
                "data": efold_fragments,
                "labelColor": "rgb(128,128,128)"
            })
        
    return specific_response


def convert_ligand_residues(generic_response):
    """
        Summary:
            Converts generic ligand binding residues response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic ligand binding residues response
        Returns:
            A protvista specific ligand binding residues response object 
    """
    
    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")

    specific_response = {}
    
    for entry_id in generic_response.keys():
        entry_data = generic_response[entry_id]

        specific_response[entry_id] = {
            "sequence": entry_data["sequence"],
            "length": entry_data["length"],
            "largeLabels": True,
            "tracks": [{
                "labelType": "text",
                "label": "Ligand binding sites",
                "data": []
            }]
        }

        for ligand in entry_data["data"]:
            ligand_fragment = {
                "color": "rgb(65, 105, 225)",
                "type": "PDB range",
                "labelType": "text",
                "tooltipContent": "Ligand binding site",
                "accession": ligand["accession"],
                "label": ligand["accession"],
                "bestChainId": ligand["additionalData"]["bestChainId"],
                "entityId": ligand["additionalData"]["entityId"],
                "locations": [{
                    "fragments": []
                }],
                "labelTooltip": "Residues binding {} <br><span style=\"display:inline-block;width:100%;text-align:center;margin-top:5px;\">"
                    "<img src=\"https://www.ebi.ac.uk/pdbe/static/files/pdbechem_v2/{}_200.svg\" /><span>".format(ligand["name"], ligand["accession"]),
                "scaffold_id": ligand["additionalData"]["scaffoldId"],
                "cofactor_id": ligand["additionalData"]["cofactorId"],
                "residue_count": ligand["additionalData"]["residueCount"]
            }

            for residue in ligand["residues"]:
                residue_fragment = {
                    "start": residue["startIndex"],
                    "end": residue["endIndex"],
                    "tooltipContent": "Type: Ligand binding site {}<br>Range: {}{} - {}{} (Chain {})<br>Ligand: {}<br>".format(residue["additionalData"]["boundMoleculeId"],
                        residue["startCode"], residue["startIndex"], residue["endCode"], residue["endIndex"], ligand["additionalData"]["bestChainId"], ligand["accession"])
                }
                ligand_fragment["locations"][0]["fragments"].append(residue_fragment)
            
            specific_response[entry_id]["tracks"][0]["data"].append(ligand_fragment)
                
    return specific_response


def convert_interface_residues(generic_response):
    """
        Summary:
            Converts generic interface residues response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic interface residues response
        Returns:
            A protvista specific interface residues response object 
    """

    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")

    specific_response = {}
    
    for entry_id in generic_response.keys():
        entry_data = generic_response[entry_id]

        specific_response[entry_id] = {
            "sequence": entry_data["sequence"],
            "length": entry_data["length"],
            "largeLabels": True,
            "tracks": [{
                "labelType": "text",
                "label": "Interaction interfaces",
                "data": []
            }]
        }

        for partner in entry_data["data"]:
            partner_fragment = {
                "color": "rgb(65, 105, 225)",
                "type": "PDB range",
                "labelType": "text",
                "tooltipContent": "Interaction interfaces",
                "accession": "{} ({})".format(partner["name"], partner["accession"]),
                "label": "{} ({})".format(partner["name"], partner["accession"]),
                "bestChainId": partner["additionalData"]["bestChainId"],
                "entityId": partner["additionalData"]["entityId"],
                "labelTooltip": "Residues interacting with {} ({})".format(partner["name"], partner["accession"]),
                "locations": [{
                    "fragments": []
                }]
            }

            for residue in partner["residues"]:
                residue_fragment = {
                    "start": residue["startIndex"],
                    "end": residue["endIndex"],
                    "tooltipContent": "Type: Interface<br>Range: {}{} - {}{} (Chain {})<br>Partner: <a href=\"https://www.ebi.ac.uk/pdbe/pdbe-kb/proteins/{}\" target=\"_blank\">"
                        "<img src=\"https://www.ebi.ac.uk/pdbe/pdbe-kb/static/icon/favicon-32x32.png\" title=\"Protein Data Bank in Europe - Knowledge Base\" "
                        "style=\"border:0;height:15px;width:15px;margin-top: 1px;\"> {}</a>".format(residue["startCode"], residue["startIndex"], residue["endCode"], residue["endIndex"], 
                            partner["additionalData"]["bestChainId"], partner["accession"], partner["accession"]
                    )
                }
                partner_fragment["locations"][0]["fragments"].append(residue_fragment)

            specific_response[entry_id]["tracks"][0]["data"].append(partner_fragment)

    return specific_response


def convert_annotations(generic_response):
    """
        Summary:
            Converts generic FunPDBe annotations response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic FunPDBe annotations response
        Returns:
            A protvista specific FunPDBe annotations response object 
    """

    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")

    specific_response = {}
    
    for entry_id in generic_response.keys():
        entry_data = generic_response[entry_id]
        
        # legend flags
        predicted_legend_added = curated_legend_added = False

        specific_response[entry_id] = {
            "sequence": entry_data["sequence"],
            "length": entry_data["length"],
            "largeLabels": True,
            "tracks": [],
            "legends": {
                "data": {
                    "Annotations": []
                },
                "alignment": "right"
            }
        }

        dict_resource = create_resource_dict(entry_data["data"])
        
        for resource in entry_data["data"]:
            accession = resource["accession"]
            
            colored_residues = resource["residues"]

            # dictionary to keep positions for a site
            dict_site = {}
            
            # dictionary to keep positions for a site
            dict_site = {}

            # assign colors only if enabled in configuration
            if funpdbe_resource_dict[accession]["assign_color"]:
                colored_residues = assign_residue_colors_for_annotations(resource["residues"], funpdbe_resource_dict[accession]["color_gradient"])
            
            shape_index = 0

            # dictionary to keep shape assigned for a siteId
            dict_shape = {}

            for residue in colored_residues:
                resource_url = residue["additionalData"]["resourceUrl"]
                raw_score = residue["additionalData"]["rawScore"]
                conf_score = residue["additionalData"]["confidenceScore"]
                raw_score_label = f"Raw score: {raw_score}<br>" if raw_score else ""
                conf_score_label = f"Confidence score: {conf_score}<br>" if conf_score else ""
                
                # handle dynamine and depth differently
                # set accession to specific accession for dynamine and depth
                if resource["accession"] in ["dynamine", "depth"]:
                    accession = residue["additionalData"]["groupLabel"]

                # handle ChannelsDB differently
                # if resource["accession"] == "ChannelsDB":
                #     if residue["additionalData"]["confidenceLevel"] == "curated":
                #         accession = "ChannelsDB_Curated"
                #     else:
                #         accession = "ChannelsDB_Predicted"

                if resource_url is None:
                    resource_url = funpdbe_resource_dict[accession]["link"]

                # store positions for a site
                if dict_site.get(residue["additionalData"]["ordinalId"]) is None:
                    dict_site[residue["additionalData"]["ordinalId"]] = [{
                        "start":residue["startIndex"],
                        "end":residue["endIndex"]
                    }]
                else:
                    dict_site[residue["additionalData"]["ordinalId"]].append({
                        "start":residue["startIndex"],
                        "end":residue["endIndex"]
                    })

                site_id = residue["additionalData"]["ordinalId"]

                residue_fragment = {
                    "start": residue["startIndex"],
                    "end": residue["endIndex"],
                    "tooltipContent": "Site id: {}<br>Type: {}<br>Label: {}<br>Residue: {}{}<br>Source: <a target=\"_blank\" href=\"{}\">{}</a><br>{}"
                        "{}Confidence level: {}".format(residue["additionalData"]["ordinalId"], funpdbe_resource_dict[accession]["label"], residue["additionalData"]["groupLabel"], residue["startCode"], residue["startIndex"],
                            resource_url, funpdbe_resource_dict[accession]["displayName"], raw_score_label,
                            conf_score_label, residue["additionalData"]["confidenceLevel"]),
                    "siteId": site_id # PDBE-2994: Keep siteId in a field to be used later
                }

                if accession in ["Total SASA", "Hydrophobic SASA", "Hydrophilic SASA"]:
                    residue_fragment["tooltipContent"] = """Site id: {}<br>Type: Solvent accessible surface area<br>Label: {}<br>PDB residue: {}{}<br>Source: <a target=\"_blank\" href=\"{}\">POPScomp</a><br>{}""".format(
                        residue["additionalData"]["ordinalId"], residue["additionalData"]["groupLabel"], residue["startCode"], residue["startIndex"], resource_url, raw_score_label)
                elif accession in ["ASA alone", "ASA in assembly"]:
                    residue_fragment["tooltipContent"] = """Site id: {}<br>Type: Solvent accessible surface area<br>Label: {}<br>PDB residue: {}{}<br>Source: <a target=\"_blank\" href=\"{}\">3DComplex</a><br>{}""".format(
                        residue["additionalData"]["ordinalId"], residue["additionalData"]["groupLabel"], residue["startCode"], residue["startIndex"], resource_url, raw_score_label)

                # PDBE-2999: Assign a shape to a siteId
                assigned_shape = None
                if dict_shape.get(site_id) is None:
                    assigned_shape = dict_shape[site_id] = protvista_allowed_shapes[shape_index]
                    shape_index += 1

                    if shape_index == len(protvista_allowed_shapes):
                        shape_index = 0

                else:
                    assigned_shape = dict_shape[site_id]
                
                residue_fragment["shape"] = assigned_shape

                if funpdbe_resource_dict[accession]["assign_color"]:
                    residue_fragment["color"] = residue["color"]

                dict_resource[accession]["locations"][0]["fragments"].append(residue_fragment)

                if dict_resource[accession].get("labelColor") is None:
                    label_color = funpdbe_resource_dict[accession].get("labelColor")

                    if label_color is not None:
                        dict_resource[accession].update({
                            "labelColor": label_color
                        })
            
            # split the locations list into fragments list if residues overlap
            dict_resource[accession]["locations"] = split_to_fragments(dict_resource[accession]["locations"][0]["fragments"])

            # for fragment in dict_resource[accession]["locations"]:
            #     for item in fragment["fragments"]:
            #         item.update({
            #             "relatedRanges": dict_site[item["siteId"]]
            #         })
            #         del item["siteId"]

        for resource_type, values in funpdbe_resource_type_dict.items():
            resource_type_fragment = {
                "label": resource_type,
                "labelType": "text",
                "data": []
            }
        
            for resource in values[0]:
                if dict_resource.get(resource) is not None:
                    resource_type_fragment["data"].append(dict_resource[resource])
            
            if resource_type_fragment["data"]:
                if "Predicted" in resource_type or resource_type == "Biophysical parameters" or resource_type == "Molecular channels":
                    resource_type_fragment["labelColor"] = color_protvista_predicted_annotations
                    predicted_legend_added = True
                else:
                    # resource_type_fragment["labelColor"] = color_protvista_curated_annotations
                    curated_legend_added = True

                specific_response[entry_id]["tracks"].append(resource_type_fragment)

        specific_response[entry_id]["tracks"] = sorted(specific_response[entry_id]["tracks"], key=lambda x: funpdbe_resource_type_dict[x["label"]][1])

        # add legends
        if predicted_legend_added:
            specific_response[entry_id]["legends"]["data"]["Annotations"].append({
                "color": color_protvista_predicted_annotations,
                "text": "Predicted annotations"
            })
        if curated_legend_added:
            specific_response[entry_id]["legends"]["data"]["Annotations"].append({
                "color": color_protvista_curated_annotations,
                "text": "Curated annotations"
            })

    return specific_response


def convert_domains(generic_response):
    """
        Summary:
            Converts generic sequence and structural domains response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic sequence and structural domains response
        Returns:
            A protvista specific sequence and structural domains response object 
    """

    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")

    specific_response = {}
    
    for entry_id in generic_response.keys():
        entry_data = generic_response[entry_id]

        specific_response[entry_id] = {
            "sequence": entry_data["sequence"],
            "length": entry_data["length"],
            "largeLabels": True,
            "tracks": [{
                "label": "Domains",
                "labelType": "text",
                "data": []
            }],
            "legends": {
                "data": {
                    "Domains": []
                },
                "alignment": "right"
            }
        }

        for domain in entry_data["data"]:
            domain_type = domain["dataType"]
            domain_fragment = {
                "color": f"rgb({domains_color_palette[domain_type][0]})",
                "type": "PDB range",
                "labelType": "text",
                "tooltipContent": domain["name"],
                "accession": domain["accession"],
                "label": domain["name"],
                "entityId": domain["additionalData"]["entityId"],
                "bestChainId": domain["additionalData"]["bestChainId"],
                "labelTooltip": "Residues mapped to {}".format(domain["name"]),
                "locations": [{
                    "fragments": []
                }]
            }

            incr = 0
            dict_domain_color = dict()

            for residue in domain["residues"]:

                href_param = residue["additionalData"]["domainId"]
                dict_key = residue["additionalData"]["domainId"]
                domain_desc = residue["additionalData"]["domainId"]
                
                if domain_type in ['CATH', 'CATH-B', 'SCOP']:
                    dict_key = (residue["additionalData"]["domainId"], residue["additionalData"]["domain"])
                    domain_desc = "{}, {}".format(residue["additionalData"]["domainId"], residue["additionalData"]["domain"])
                    href_param = residue["additionalData"]["domain"]    

                # if there is color assigned to a domainId, then assign same color to temp_color
                if dict_domain_color.get(dict_key) is not None:
                    temp_color = dict_domain_color[dict_key]
                else:
                    temp_color = domains_color_palette[domain_type][incr]
                    dict_domain_color[dict_key] = temp_color
                    incr += 1
            
                if incr == len(domains_color_palette[domain_type]):
                    incr = 0
        
                residue_fragment = {
                    "color": f"rgb({temp_color})",
                    "start": residue["startIndex"],
                    "end": residue["endIndex"],
                    "tooltipContent": "Type: {}<br>Range: {}{} - {}{}<br>Source: <a target=\"_blank\" href=\"{}{}\">{} ({})</a>".format(
                        domain["name"].rstrip("s"), residue["startCode"], residue["startIndex"], residue["endCode"], residue["endIndex"], domains_dict[domain_type][1], 
                        href_param, residue["additionalData"]["domainName"], domain_desc
                    )
                }
                domain_fragment["locations"][0]["fragments"].append(residue_fragment)
            
                # delete the dictionary, it can happen that same domainId comes for different type of domains. For eg. CATH and CATH-B
            del dict_domain_color
            specific_response[entry_id]["tracks"][0]["data"].append(domain_fragment)

            # add legend
            specific_response[entry_id]["legends"]["data"]["Domains"].append({
                "color": list(map(lambda x: "rgb({})".format(x), domains_color_palette[domain_type])),
                "text": domain["name"]
            })

    return specific_response


def convert_chains(generic_response):
    """
        Summary:
            Converts generic chains response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic chains response
        Returns:
            A protvista specific chains response object 
    """

    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")

    specific_response = {}
    
    for entry_id in generic_response.keys():
        entry_data = generic_response[entry_id]
        dict_legends = dict()

        specific_response[entry_id] = {
            "sequence": entry_data["sequence"],
            "length": entry_data["length"],
            "largeLabels": True,
            "tracks": [{
                "label": "Chains",
                "labelType": "text",
                "data": []
            }],
            "legends": {
                "data": {
                    "Quality": []
                },
                "alignment": "right"
            }
        }

        # PDBE-2673: control visibility of legends
        missing_legend_added = False
        good_legend_added = False
        low_legend_added = False
        medium_legend_added = False
        high_legend_added = False

        for element in entry_data["data"]:
            element_fragment = {
                "type": "PDB",
                "labelType": "text",
                "tooltipContent": element["name"],
                "accession": element["accession"],
                "label": f"""{element["name"]} (auth {element["additionalData"]["authAsymId"]})""",
                "labelTooltip": element["name"],
                "entityId": element["additionalData"]["entityId"],
                "bestChainId": element["additionalData"]["bestChainId"],
                "chainId": element["additionalData"]["chainId"],
                "locations": [{
                    "fragments": []
                }]
            }

            if element["additionalData"].get("shape"):
                element_fragment.update({
                    "shape": element["additionalData"]["shape"]
                })
            
            for residue in element["residues"]:
                validation_label = residue["additionalData"]["validationIssue"]

                residue_fragment = {
                    "start": residue["startIndex"],
                    "end": residue["endIndex"],
                    "tooltipContent": "{}<br>Residue: {}{}".format(quality_details[residue["additionalData"]["qualityLevel"]][1], residue["startCode"], residue["startIndex"]),
                    "color": quality_details[residue["additionalData"]["qualityLevel"]][0]
                }

                if validation_label != "RSRZ":
                    residue_fragment["tooltipContent"] += f"<br>{validation_label}<br>"

                element_fragment["locations"][0]["fragments"].append(residue_fragment)
                
                # adding legends based on quality level

                quality_level = residue["additionalData"]["qualityLevel"]
                if quality_level == "high":
                    high_legend_added = True
                elif quality_level == "good":
                    good_legend_added = True
                elif quality_level == "medium":
                    medium_legend_added = True
                elif quality_level == "low":
                    low_legend_added = True
                elif quality_level == "medium":
                    medium_legend_added = True
                elif quality_level == "missing":
                    missing_legend_added = True

            specific_response[entry_id]["tracks"][0]["data"].append(element_fragment)

        # add legends
        if high_legend_added:
            specific_response[entry_id]["legends"]["data"]["Quality"].append({
                "color": quality_details["high"][0],
                "text": quality_details["high"][1]
            })
        if good_legend_added:
            specific_response[entry_id]["legends"]["data"]["Quality"].append({
                "color": quality_details["good"][0],
                "text": quality_details["good"][1]
            })
        if medium_legend_added:
            specific_response[entry_id]["legends"]["data"]["Quality"].append({
                "color": quality_details["medium"][0],
                "text": quality_details["medium"][1]
            })
        if low_legend_added:
            specific_response[entry_id]["legends"]["data"]["Quality"].append({
                "color": quality_details["low"][0],
                "text": quality_details["low"][1]
            })
        if missing_legend_added:
            specific_response[entry_id]["legends"]["data"]["Quality"].append({
                "color": quality_details["missing"][0],
                "text": quality_details["missing"][1]
            })

    return specific_response


def convert_variation(generic_response):
    """
        Summary:
            Converts generic variation response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic variation response
        Returns:
            A protvista specific variation response object 
    """

    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")
    
    specific_response = {}

    for entry_id in generic_response.keys():
        entry_data = generic_response[entry_id]
        specific_response["sequence"] = entry_data["sequence"]
        specific_response["variants"] = []

        for accession, results in entry_data["data"].items():
            for feature in results["features"]:
                if feature.get("clinicalSignificances") is None:
                    feature["clinicalSignificances"] = None

                variant_fragment = {
                    "accession": feature["genomicLocation"],
                    "association": [] if feature.get("association") is None else feature["association"],
                    "clinicalSignificances": feature["clinicalSignificances"],
                    "color": get_variation_color(feature),
                    "end": feature["end"],
                    "polyphenScore": feature["polyphenScore"],
                    "siftScore": feature["siftScore"],
                    "sourceType": feature["sourceType"],
                    "start": feature["begin"],
                    "tooltipContent": format_variation_tooltip(feature),
                    "variant": feature["alternativeSequence"],
                    "xrefNames": [] if feature.get("xrefs") is None else get_source_type(feature["xrefs"], feature["sourceType"])
                }

                variant_fragment["keywords"] = get_variation_filter_keywords(variant_fragment)

                specific_response["variants"].append(variant_fragment)
        
    return specific_response


def convert_rfam(generic_response):
    """
        Summary:
            Converts generic rfam domains response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic rfam domains response
        Returns:
            A protvista specific rfam domains response object 
    """

    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")

    specific_response = {}
    
    for entry_id in generic_response.keys():
        entry_data = generic_response[entry_id]

        specific_response[entry_id] = {
            "sequence": entry_data["sequence"],
            "length": entry_data["length"],
            "largeLabels": True,
            "tracks": [{
                "label": "Rfam",
                "labelType": "text",
                "data": []
            }],
            "legends": {
                "data": {
                    "Rfam Domains": []
                },
                "alignment": "right"
            }
        }

        for domain in entry_data["data"]:
            domain_type = domain["dataType"]
            domain_fragment = {
                "color": f"rgb({domains_color_palette[domain_type][0]})",
                "type": "PDB range",
                "labelType": "text",
                "tooltipContent": domain["name"],
                "accession": domain["accession"],
                "label": domain["name"],
                "entityId": domain["additionalData"]["entityId"],
                "bestChainId": domain["additionalData"]["bestChainId"],
                "labelTooltip": "Residues mapped to {}".format(domain["name"]),
                "locations": [{
                    "fragments": []
                }]
            }

            incr = 0
            dict_domain_color = dict()

            for residue in domain["residues"]:
                
                # if there is color assigned to a domainId, then assign same color to temp_color
                if dict_domain_color.get(residue["additionalData"]["domainId"]) is not None:
                    temp_color = dict_domain_color[residue["additionalData"]["domainId"]]
                else:
                    temp_color = domains_color_palette[domain_type][incr]
                    dict_domain_color[residue["additionalData"]["domainId"]] = temp_color
                    incr += 1
            
                if incr == len(domains_color_palette[domain_type]):
                    incr = 0
        
                residue_fragment = {
                    "color": f"rgb({temp_color})",
                    "start": residue["startIndex"],
                    "end": residue["endIndex"],
                    "tooltipContent": "Type: {}<br>Range: {}{} - {}{}<br>Source: <a target=\"_blank\" href=\"{}{}\">{} ({})</a>".format(
                        domain["name"].rstrip("s"), residue["startCode"], residue["startIndex"], residue["endCode"], residue["endIndex"], domains_dict[domain_type][1], 
                        residue["additionalData"]["domainId"], residue["additionalData"]["domainName"], residue["additionalData"]["domainId"]
                    )
                }
                domain_fragment["locations"][0]["fragments"].append(residue_fragment)
            
                # delete the dictionary, it can happen that same domainId comes for different type of domains. For eg. CATH and CATH-B
            del dict_domain_color
            specific_response[entry_id]["tracks"][0]["data"].append(domain_fragment)

            # add legend
            specific_response[entry_id]["legends"]["data"]["Rfam Domains"].append({
                "color": list(map(lambda x: "rgb({})".format(x), domains_color_palette[domain_type])),
                "text": domain["name"]
            })

    return specific_response