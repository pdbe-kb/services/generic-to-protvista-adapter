


from typing import NamedTuple


class RGB(NamedTuple):
    r: float
    g: float
    b: float

    def __str__(self) -> str:
        r = int(round(self.r))
        g = int(round(self.g))
        b = int(round(self.b))
        return f'rgb({r},{g},{b})'


color_protvista_interaction_interfaces = "rgb(65, 105, 225)"
color_protvista_ligand_sites = "rgb(65, 105, 225)"
color_protvista_predicted_annotations = "rgb(128,128,128)"
color_protvista_curated_annotations = "rgb(65, 105, 225)"
color_protvista_interaction_interfaces_shades = [
                                                    "rgb(217,225,249)", "rgb(201,212,246)", "rgb(184,199,244)", "rgb(167,185,241)", "rgb(150,172,238)", "rgb(133,158,236)", 
                                                    "rgb(116,145,233)", "rgb(99,132,230)", "rgb(82,118,228)", "rgb(65,105,225)", "rgb(48,92,222)"
                                                ]

color_protvista_cafactor_shades = [
                                                    "rgb(139,241,139)", "rgb(125,227,125)", "rgb(111,213,111)", "rgb(97,199,97)", "rgb(83,185,83)", 
                                                    "rgb(70,172,70)", "rgb(56,158,56)", "rgb(42,144,42)", "rgb(28,130,28)", "rgb(14,116,14)", "rgb(0,102,0)"
                                                ]

protvista_allowed_shapes = ["rectangle","bridge","diamond","chevron","catFace","triangle","wave","hexagon","pentagon","circle","arrow","doubleBar"]

color_protvista_processed_proteins = "rgb(135,158,247)"

color_residue_conflicts = "rgb(255,121,0)"

color_protvista_interaction_interfaces_shades = [
                                                    "rgb(217,225,249)", "rgb(201,212,246)", "rgb(184,199,244)", "rgb(167,185,241)", "rgb(150,172,238)", "rgb(133,158,236)", 
                                                    "rgb(116,145,233)", "rgb(99,132,230)", "rgb(82,118,228)", "rgb(65,105,225)", "rgb(48,92,222)"
                                                ]
protvista_allowed_shapes = ["rectangle","bridge","diamond","chevron","catFace","triangle","wave","hexagon","pentagon","circle","arrow","doubleBar"]

color_protvista_processed_proteins = "rgb(135,158,247)"

color_residue_conflicts = "rgb(255,121,0)"

funpdbe_resource_dict = {
    "canSAR": {
        "color": "rgb(128,128,128)",
        "shape": "arrow",
        "label": "Predicted ligand binding sites",
        "displayName": "canSAR",
        "desc": "canSAR is an integrated knowledge-base to provide drug-discovery useful predictions",
        "link": "https://cansarblack.icr.ac.uk/",
        "labelColor": "rgb(211,211,211)",
        "assign_color": False,
        "allow_site_shapes": True
    },
    "cath-funsites": {
        "color": "rgb(128,128,128)",
        "shape": "circle",
        "label": "Predicted functional sites",
        "displayName": "CATH-FunSites",
        "desc": "CATH-FunSites are groups of amino acid residues that are evolutionarily conserved within a CATH FunFam",
        "link": "https://www.cathdb.info/",
        "labelColor": "rgb(211,211,211)",
        "assign_color": False,
        "allow_site_shapes": True
    },
    "akid": {
        "color": "rgb(128,128,128)",
        "shape": "triangle",
        "label": "Predicted PTM sites",
        "displayName": "AKID",
        "desc": "Predictions of phosphorylation sites for kinases in target proteins",
        "link": "http://akid.bio.uniroma2.it/",
        "labelColor": "rgb(211,211,211)",
        "assign_color": False,
        "allow_site_shapes": True
    },
    "3dligandsite": {
        "color": "rgb(128,128,128)",
        "shape": "arrow",
        "label": "Predicted ligand binding sites",
        "displayName": "3DLigandSite",
        "desc": "3DLigandSite is an automated method for the prediction of ligand binding sites",
        "link": "http://www.sbg.bio.ic.ac.uk/~3dligandsite/",
        "labelColor": "rgb(211,211,211)",
        "assign_color": False,
        "allow_site_shapes": True
    },
    "ProKinO": {
        "color": "rgb(65, 105, 225)",
        "shape": "triangle",
        "label": "Curated regularity motifs",
        "displayName": "ProKinO",
        "desc": "The Protein Kinase Ontology (ProKinO) is a protein kinase-specific ontology",
        "link": "https://prokino.uga.edu/",
        "assign_color": False,
        "allow_site_shapes": True
    },
    "14-3-3-pred": {
        "color": "rgb(128,128,128)",
        "shape": "triangle",
        "label": "Predicted protein binding sites",
        "displayName": "14-3-3-Pred",
        "desc": "14-3-3-Pred is a webserver that predicts 14-3-3-binding sites in proteins",
        "link": "https://www.compbio.dundee.ac.uk/1433pred",
        "labelColor": "rgb(211,211,211)",
        "assign_color": False,
        "allow_site_shapes": True
    },
    "nod": {
        "color": "rgb(128,128,128)",
        "shape": "triangle",
        "label": "Predicted PTM sites",
        "displayName": "NoD",
        "desc": "NoD is a predictor of nucleolar localisation sequences (NoLSs) in proteins",
        "link": "https://www.compbio.dundee.ac.uk/www-nod/",
        "labelColor": "rgb(211,211,211)",
        "assign_color": False,
        "allow_site_shapes": True
    },
    "M-CSA": {
        "color": "rgb(65, 105, 225)",
        "shape": "triangle",
        "label": "Curated catalytic sites",
        "displayName": "M-CSA",
        "desc": "M-CSA is a database of enzyme reaction mechanisms",
        "link": "https://www.ebi.ac.uk/thornton-srv/m-csa/",
        "assign_color": False,
        "allow_site_shapes": True
    },
    "p2rank": {
        "color": "rgb(128,128,128)",
        "shape": "arrow",
        "label": "Predicted ligand binding sites",
        "displayName": "P2Rank",
        "desc": "P2Rank is a new open source software package for ligand binding site prediction from protein structure",
        "link": "https://prankweb.cz/",
        "labelColor": "rgb(211,211,211)",
        "assign_color": False,
        "allow_site_shapes": True
    },
    "ChannelsDB": {
        "color": "rgb(128,128,128)",
        "shape": "arrow",
        "label": "Molecular channels",
        "displayName": "ChannelsDB",
        "desc": "ChannelsDB is a database providing information about the positions, geometry and physicochemical properties of channels (pores and tunnels) found within biomacromolecular structures deposited in the Protein Data Bank",
        "link": "https://channelsdb2.biodata.ceitec.cz/",
        "assign_color": False,
        "labelColor": "rgb(211,211,211)",
        "allow_site_shapes": True
    },
    "ChannelsDB_Curated": {
        "color": "rgb(128,128,128)",
        "shape": "arrow",
        "label": "Curated molecular channels",
        "displayName": "ChannelsDB",
        "desc": "ChannelsDB is a database providing information about the positions, geometry and physicochemical properties of channels (pores and tunnels) found within biomacromolecular structures deposited in the Protein Data Bank",
        "link": "https://channelsdb2.biodata.ceitec.cz/",
        "assign_color": False,
        "allow_site_shapes": True
    },
    "ChannelsDB_Predicted": {
        "color": "rgb(128,128,128)",
        "shape": "arrow",
        "label": "Predicted molecular channels",
        "displayName": "ChannelsDB",
        "desc": "ChannelsDB is a database providing information about the positions, geometry and physicochemical properties of channels (pores and tunnels) found within biomacromolecular structures deposited in the Protein Data Bank",
        "link": "https://channelsdb2.biodata.ceitec.cz/",
        "assign_color": False,
        "allow_site_shapes": True
    },
    "camkinet": {
        "color": "rgb(65, 105, 225)",
        "shape": "triangle",
        "label": "Curated PTM sites",
        "displayName": "CaMKinet",
        "desc": "CaMKinet is a database providing infomation on CaMKs. Calcium/Calmodulin-dependent protein kinases (CaMKs) and their allies constitute a large family of Serine/Threonine kinases",
        "link": "http://camkinet.embl.de/v2/home/",
        "assign_color": False,
        "allow_site_shapes": True
    },
    "depth": {
        "color": "rgb(128,128,128)",
        "shape": "rectangle",
        "label": "Biophysical parameters",
        "displayName": "Cospi-Depth",
        "desc": "Cospi-Depth is a server for computing/predicting depth, cavity sizes, ligand binding sites and pKa",
        "link": "http://cospi.iiserpune.ac.in/depth/htdocs/index.html",
        "assign_color": True,
        "color_gradient": "grey",
        "labelColor": "rgb(211,211,211)"
    },
    "dynamine": {
        "color": "rgb(128,128,128)",
        "shape": "rectangle",
        "label": "Biophysical parameters",
        "displayName": "DynaMine",
        "desc": "DynaMine is a fast predictor of protein backbone dynamics using only sequence information as input. Higher scores indicate stable backbone, while lower scores predict flexibility.",
        "link": "http://dynamine.ibsquare.be/",
        "labelColor": "rgb(211,211,211)",
        "assign_color": True,
        "color_gradient": "grey"
    },
    "efoldmine": {
        "color": "rgb(128,128,128)",
        "shape": "rectangle",
        "label": "Biophysical parameters",
        "displayName": "EFoldMine",
        "desc": "EFoldMine is a fast predictor of early folding regions using only sequence information as input",
        "link": "http://dynamine.ibsquare.be/",
        "labelColor": "rgb(211,211,211)",
        "assign_color": True,
        "color_gradient": "grey"
    },
    "backbone": {
        "color": "rgb(128,128,128)",
        "shape": "rectangle",
        "label": "Biophysical parameters",
        "displayName": "DynaMine",
        "desc": "DynaMine is a fast predictor of protein backbone dynamics using only sequence information as input. Higher scores indicate stable backbone, while lower scores predict flexibility.",
        "link": "http://dynamine.ibsquare.be/",
        "labelColor": "rgb(211,211,211)",
        "assign_color": True,
        "color_gradient": "grey"
    },
    "complex_residue_depth": {
        "color": "rgb(128,128,128)",
        "shape": "rectangle",
        "label": "Biophysical parameters",
        "displayName": "Complex Residue Depth",
        "desc": "Cospi-Depth is a server for computing/predicting depth, cavity sizes, ligand binding sites and pKa",
        "link": "http://cospi.iiserpune.ac.in/depth/htdocs/index.html",
        "assign_color": True,
        "color_gradient": "grey",
        "labelColor": "rgb(211,211,211)"
    },
    "monomeric_residue_depth": {
        "color": "rgb(128,128,128)",
        "shape": "rectangle",
        "label": "Biophysical parameters",
        "displayName": "Monomeric Residue Depth",
        "desc": "Cospi-Depth is a server for computing/predicting depth, cavity sizes, ligand binding sites and pKa",
        "link": "http://cospi.iiserpune.ac.in/depth/htdocs/index.html",
        "assign_color": True,
        "color_gradient": "grey",
        "labelColor": "rgb(211,211,211)"
    },
    "MetalPDB": {
        "color": "rgb(65, 105, 225)",
        "shape": "arrow",
        "label": "Curated metal binding sites",
        "displayName": "MetalPDB",
        "desc": "MetalPDB is a database of metal sites in biological macromolecular structures",
        "link": "https://metalpdb.cerm.unifi.it/",
        "assign_color": False,
        "allow_site_shapes": True
    },
    "FoldX": {
        "color": "rgb(128,128,128)",
        "shape": "triangle",
        "label": "Predicted protein binding sites",
        "displayName": "FoldX",
        "desc": "FoldX provides a fast and quantitative estimation of the importance of the interactions contributing to the stability of proteins and protein complexes",
        "link": "http://foldxsuite.crg.eu/",
        "labelColor": "rgb(211,211,211)",
        "assign_color": False,
        "allow_site_shapes": True
    },
    "3Dcomplex": {
        "color": "rgb(65, 105, 225)",
        "shape": "triangle",
        "label": "Curated interface annotations",
        "displayName": "3DComplex",
        "desc": "3D Complex is a hierarchical classification of protein complexes that describes similarities in structure, sequence, as well as topology of contacts of the constituent proteins",
        "link": "https://shmoo.weizmann.ac.il/elevy/3dcomplexV6/Home.cgi",
        "assign_color": False,
        "allow_site_shapes": True
    },
    "Covalentizer": {
        "color": "rgb(128,128,128)",
        "shape": "arrow",
        "label": "Predicted ligand binding sites",
        "displayName": "Covalentizer",
        "desc": "Covalentizer is a computational pipeline for creating irreversible inhibitors, starting from X-ray structures of known reversible binders.",
        "link": "https://covalentizer.weizmann.ac.il/covb/main",
        "labelColor": "rgb(211,211,211)",
        "assign_color": False,
        "allow_site_shapes": True
    },
    "Scop3P": {
        "color": "rgb(128,128,128)",
        "shape": "triangle",
        "label": "Predicted PTM sites",
        "displayName": "Scop3P",
        "desc": "A Comprehensive Resource of Human Phosphosites within Their Full Context",
        "link": "https://iomics.ugent.be/scop3p/index",
        "labelColor": "rgb(211,211,211)",
        "assign_color": False,
        "allow_site_shapes": True
    },
    "Total SASA": {
        "color": "rgb(128,128,128)",
        "shape": "rectangle",
        "label": "Biophysical parameters",
        "displayName": "Total SASA",
        "desc": "POPSCOMP is a method to analyse interactions between individual complex components of proteins and/or nucleic acids by calculating the solvent accessible surface area (SASA) buried upon complex formation.",
        "link": "https://github.com/Fraternalilab/POPScomp",
        "labelColor": "rgb(211,211,211)",
        "assign_color": True,
        "color_gradient": "grey"
    },
    "Hydrophilic SASA": {
        "color": "rgb(128,128,128)",
        "shape": "rectangle",
        "label": "Biophysical parameters",
        "displayName": "Hydrophilic SASA",
        "desc": "POPSCOMP is a method to analyse interactions between individual complex components of proteins and/or nucleic acids by calculating the solvent accessible surface area (SASA) buried upon complex formation.",
        "link": "https://github.com/Fraternalilab/POPScomp",
        "labelColor": "rgb(211,211,211)",
        "assign_color": True,
        "color_gradient": "grey"
    },
    "Hydrophobic SASA": {
        "color": "rgb(128,128,128)",
        "shape": "rectangle",
        "label": "Biophysical parameters",
        "displayName": "Hydrophobic SASA",
        "desc": "POPSCOMP is a method to analyse interactions between individual complex components of proteins and/or nucleic acids by calculating the solvent accessible surface area (SASA) buried upon complex formation.",
        "link": "https://github.com/Fraternalilab/POPScomp",
        "labelColor": "rgb(211,211,211)",
        "assign_color": True,
        "color_gradient": "grey"
    },
    "ASA alone": {
        "color": "rgb(128,128,128)",
        "shape": "rectangle",
        "label": "Biophysical parameters",
        "displayName": "ASA alone",
        "desc": "3D Complex is a hierarchical classification of protein complexes that describes similarities in structure, sequence, as well as topology of contacts of the constituent proteins",
        "link": "https://shmoo.weizmann.ac.il/elevy/3dcomplexV6/Home.cgi",
        "labelColor": "rgb(211,211,211)",
        "assign_color": True,
        "allow_site_shapes": True,
        "color_gradient": "grey",
    },
    "ASA in assembly": {
        "color": "rgb(128,128,128)",
        "shape": "rectangle",
        "label": "Biophysical parameters",
        "displayName": "ASA in assembly",
        "desc": "3D Complex is a hierarchical classification of protein complexes that describes similarities in structure, sequence, as well as topology of contacts of the constituent proteins",
        "link": "https://shmoo.weizmann.ac.il/elevy/3dcomplexV6/Home.cgi",
        "labelColor": "rgb(211,211,211)",
        "assign_color": True,
        "allow_site_shapes": True,
        "color_gradient": "grey",
    },
    "Mapq": {
        "color": "rgb(65, 105, 225)",
        "shape": "rectangle",
        "label": "EM validation",
        "displayName": "Mapq",
        "desc": "EMVS provides different quality and validation scores about cryo-Electron Microscopy maps and their corresponding atomic models.",
        "link": "https://3dbionotes-ws.cnb.csic.es/",
        "assign_color": True,
        "allow_site_shapes": True,
        "color_gradient": "quality",
    },
    "Monores": {
        "color": "rgb(65, 105, 225)",
        "shape": "rectangle",
        "label": "EM validation",
        "displayName": "Monores",
        "desc": "EMVS provides different quality and validation scores about cryo-Electron Microscopy maps and their corresponding atomic models.",
        "link": "https://3dbionotes-ws.cnb.csic.es/",
        "assign_color": True,
        "allow_site_shapes": True,
        "color_gradient": "resolution",
    },
    "Blocres": {
        "color": "rgb(65, 105, 225)",
        "shape": "rectangle",
        "label": "EM validation",
        "displayName": "Blocres",
        "desc": "EMVS provides different quality and validation scores about cryo-Electron Microscopy maps and their corresponding atomic models.",
        "link": "https://3dbionotes-ws.cnb.csic.es/",
        "assign_color": True,
        "allow_site_shapes": True,
        "color_gradient": "resolution",
    },
    "Deepres": {
        "color": "rgb(65, 105, 225)",
        "shape": "rectangle",
        "label": "EM validation",
        "displayName": "Deepres",
        "desc": "EMVS provides different quality and validation scores about cryo-Electron Microscopy maps and their corresponding atomic models.",
        "link": "https://3dbionotes-ws.cnb.csic.es/",
        "assign_color": True,
        "allow_site_shapes": True,
        "color_gradient": "resolution",
    },
    "Fscq": {
        "color": "rgb(65, 105, 225)",
        "shape": "rectangle",
        "label": "EM validation",
        "displayName": "Fscq",
        "desc": "EMVS provides different quality and validation scores about cryo-Electron Microscopy maps and their corresponding atomic models.",
        "link": "https://3dbionotes-ws.cnb.csic.es/",
        "assign_color": True,
        "allow_site_shapes": True,
        "color_gradient": "quality",
    },
    "FireProt DB": {
        "link": "https://loschmidt.chemi.muni.cz/test/",
    }
}

# dictionary of funpdbe resource types holds a tuple of (list of resources, sort order in result list)
funpdbe_resource_type_dict = {
    "Predicted functional sites": (["cath-funsites"], 7),
    "Predicted ligand binding sites": (["canSAR", "3dligandsite", "p2rank", "Covalentizer"], 10),
    "Predicted PTM sites": (["nod", "akid", "Scop3P"], 8),
    "Curated PTM sites": (["camkinet"], 2),
    "Curated regularity motifs": (["ProKinO"], 3),
    "Curated catalytic sites": (["M-CSA"], 1),
    # "Curated molecular channels": (["ChannelsDB_Curated"], 4),
    # "Predicted molecular channels": (["ChannelsDB_Predicted"], 11),
    "Molecular channels": (["ChannelsDB"], 11),
    "Biophysical parameters": (["complex_residue_depth", "monomeric_residue_depth", "backbone", "efoldmine", "depth", "Total SASA", "Hydrophobic SASA", "Hydrophilic SASA", "ASA alone", "ASA in assembly"], 12),
    "Predicted protein binding sites": (["14-3-3-pred"], 9),
    "Curated metal binding sites": (["MetalPDB"], 5),
    "Curated interface annotations": (["3Dcomplex"], 6),
    "EM validation": (["Monores", "Deepres", "Blocres", "Mapq", "Fscq"], 13),
}


domains_dict = {
    "Pfam": ["rgb(135,158,247)", "https://pfam.xfam.org/family/"],
    "CATH": ["rgb(107,119,39)", "http://www.cathdb.info/version/latest/domain/"],
    "SCOP": ["rgb(255,174,0)", "http://scop.mrc-lmb.cam.ac.uk/legacy/search.cgi?ver=1.75&search_type=scop&key="],
    "CATH-B": ["rgb(255,0,131)", "http://www.cathdb.info/version/latest/domain/"],
    "Rfam": ["rgb(107,119,39)", "http://rfam.xfam.org/family/"],
    "InterPro": ["rgb(10,112,163)", "https://www.ebi.ac.uk/interpro/entry/InterPro/"]
}

common_color_codes = {
    "green": "rgb(116,179,96)",
    "grey": "rgb(211,211,211)",
    "Helix": "rgb(255,99,163)",
    "Strand": "rgb(255,204,0)",
    "MobiDB": "rgb(128,128,128)",
    "KnotProt": "rgb(65, 105, 225)",
    "WEBnma": "rgb(128,128,128)",
    "KinCore": "rgb(65, 105, 225)",
    "dynamine": "rgb(128,128,128)",
    "efoldmine": "rgb(128,128,128)",
}

domains_color_palette = {
    "CATH": ["34,103,84","17,122,101","34,153,84","60,179,113","50,205,50","26,188,156","88,214,141","127,255,0","0,250,154","152,251,152","204,255,125","171,235,198","154,205,50",
             "143,188,143","128,128,0","107,128,0"],
    "CATH-B": ["255,0,0","255,86,90","250,61,105","245,32,124","248,105,167","255,153,204","237,133,133","204,102,102","194,79,79","215,33,83","204,102,153","195,155,211",
               "173,88,173","214,56,164","167,25,121","118,68,138","182,46,46"],
    "SCOP": ["255,174,0", "219,151,4", "178,130,26", "132,100,30", "102,78,26"],
    "Pfam": ["135,158,247","123,134,183","70,80,122","55,79,183","31,97,141","41,128,185","52,152,219","212,230,241","127,179,213","176,224,230","169,204,227","174,214,241",
             "84,153,199","93,173,226","214,234,248","46,134,193"],
    "Rfam": ["141,151,195", "120,133,192", "98,115,189", "77,97,186", "55,79,183"],
    "InterPro": ["255,255,0","255,153,0","255,51,0","204,153,0","204,102,0","204,51,0","153,204,0","153,153,0","153,0,0","51,51,0","0,153,51","153,153,102","51,51,153","255,153,153","255,204,204"]
}

# quality color and description for differnt quality levels
quality_details = {
    "high": ("rgb(0,182,0)", "High quality"),
    "good": ("rgb(255,255,75)", "Good quality"),
    "medium": ("rgb(255,121,0)", "Medium quality"),
    "low": ("rgb(255,0,0)", "Low quality"),
    "missing": ("rgb(150,150,150)", "Missing residue"),
    "rsrz": ("rgb(255,0,0)", "RSRZ")
}


variation_scale_colors = {
  "up_disease_color": "#990000",
  "up_non_disease_color": "#99cc00",
  "deleterious_color": "#002594",
  "benign_color": "#8FE3FF",
  "others_color": "#009e73"
}

variation_consequences = {
    "likely_disease": ["disease", "pathogenic", "risk factor"],
    "likely_benign": ["benign"],
    "uncertain": ["uncertain", "conflicting", "unclassified"]
}

# RGB codes
color_codes = {
    "helix": RGB(255, 99, 163),
    "strand": RGB(255, 204, 0),
    "loop": RGB(158, 158, 158),
}

sec_structure_display_order = ["helix", "loop", "strand"]