
import copy
from typing import List
from protvista_adapter.common_params import funpdbe_resource_dict, variation_consequences, variation_scale_colors, RGB
from prodict import Prodict

"""
Common utility functions
"""

GREY_GRADIENT = ["rgb(182,184,186)", "rgb(170,172,173)", "rgb(157,160,161)", "rgb(141,144,145)", "rgb(129,132,133)", "rgb(114,116,117)", "rgb(106,109,110)", "rgb(99,101,102)", "rgb(87,88,89)", "rgb(72,73,74)", "rgb(69,70,71)"]
BLUE_GRADIENT = ["rgb(204, 204, 255)", "rgb(179, 179, 255)", "rgb(153, 153, 255)", "rgb(128, 128, 255)", "rgb(102, 102, 255)", "rgb(77, 77, 255)", "rgb(51, 51, 255)", "rgb(26, 26, 255)", "rgb(0, 0, 204)", "rgb(0, 0, 128)", "rgb(0, 0, 102)"]
PREDICTION_GRADIENT = ["#002594", "#0D369E", "#1A48A7", "#2759B1", "#346ABB", "#417BC5", "#4E8DCE", "#5B9ED8", "#68AFE2", "#75C0EC", "#82D2F5", "#8FE3FF"]
QUALITY_GRADIENT = ["rgb(255,0,0)", "rgb(232,23,0)", "rgb(209,46,0)", "rgb(185,70,0)", "rgb(162,93,0)", "rgb(139,116,0)", "rgb(116,139,0)", "rgb(93,162,0)", "rgb(70,185,0)", "rgb(46,209,0)", "rgb(23,232,0)", "rgb(0,255,0)"]
RESOLUTION_GRADIENT = ["rgb(255, 152, 233)", "rgb(193, 67, 237)", "rgb(0, 0, 255)", "rgb(0, 255, 255)", "rgb(0, 255, 0)", "rgb(255, 255, 0)", "rgb(255, 136, 0)", "rgb(255, 0, 0)", "rgb(197, 197, 197)"]

def get_raw_scores(residues):
    
    raw_scores = []
    
    for residue in residues:
        if residue.get("additionalData"):
            raw_score = residue["additionalData"]["rawScore"]
        else:
            raw_score = residue["pdbEntries"][0]["additionalData"]["rawScore"]

        raw_scores.append(raw_score)

    return raw_scores

def assign_residue_colors_for_annotations(residues, color_gradient, inverted=False):
    """
    Assigns color to residues 
    """

    raw_scores = get_raw_scores(residues)
    
    # grey is default gradient
    color = GREY_GRADIENT

    if color_gradient == "blue":
        color = BLUE_GRADIENT
    elif color_gradient == "quality":
        color = QUALITY_GRADIENT
    elif color_gradient == "resolution":
        color = RESOLUTION_GRADIENT
        
    if inverted:
        color = color[::-1]

    for residue in residues:
        raw_score = residue["additionalData"]["rawScore"] if residue.get("additionalData") else residue["pdbEntries"][0]["additionalData"]["rawScore"]
        try:
            normalized = int((raw_score - min(raw_scores))/(max(raw_scores) - min(raw_scores)) * 10)
        except ZeroDivisionError:
            normalized = 1
        
        if normalized >= len(color):
            normalized = len(color) - 1

        residue["color"] = color[normalized]

    return residues


def split_to_fragments(fragments):
    """
    Checks residue numbers and split them to fragments if same residue number occurs multiple times
    """

    locations = []
    dict_residues = {}

    for fragment in fragments:
        if dict_residues.get(fragment["start"]) is None:
            dict_residues[fragment["start"]] = [fragment]
        else:
            dict_residues[fragment["start"]].append(fragment)
    
    for key in sorted(dict_residues.keys()):
        incr = 0
        
        for fragment in dict_residues[key]:
            if len(locations) <= incr:
                locations.append({})

            if locations[incr].get("fragments") is None:
                locations[incr]["fragments"] = [fragment]
            else:
                locations[incr]["fragments"].append(fragment)
            incr += 1

    return locations


def create_resource_dict(resources):

    dict_resource = {}

    for resource in resources:
        # handle dynamine and depth differently
        if resource["accession"] in ["dynamine", "depth"]:
            for residue in resource["residues"]:
                accession = residue["additionalData"]["groupLabel"]

                if dict_resource.get(accession) is None:
                    dict_resource[accession] = get_resource_dict_fragment(resource, accession)
                    
        # elif resource["accession"] == "ChannelsDB":
        #     for residue in resource["residues"]:
        #         if residue["additionalData"]["confidenceLevel"] == "curated":
        #             accession = "ChannelsDB_Curated"
        #         else:
        #             accession = "ChannelsDB_Predicted"

        #         if dict_resource.get(accession) is None:
        #             dict_resource[accession] = get_resource_dict_fragment(resource, accession)
        else:
            dict_resource[resource["accession"]] = get_resource_dict_fragment(resource, resource["accession"])

    return dict_resource


def get_resource_dict_fragment(resource, accession):
    return {
        "color": funpdbe_resource_dict[accession]["color"],
        "type": "PDB",
        "labelType": "text",
        "tooltipContent": resource["name"],
        "accession": accession,
        "bestChainId": resource["additionalData"]["bestChainId"],
        "entityId": resource["additionalData"]["entityId"],
        "label": "<a target=\"_blank\" href=\"{}\">{} <i class=\"icon icon-generic\" style=\"font-size:75%\" data-icon=\"x\"></i></a>".format(funpdbe_resource_dict[accession]["link"],
            funpdbe_resource_dict[accession]["displayName"]),
        "labelTooltip": funpdbe_resource_dict[accession]["desc"],
        "locations": [{
            "fragments": []
        }],
        # "shape": funpdbe_resource_dict[accession]["shape"] # PDBE-2999: Commented, else will override fragments shape
        }


def get_variation_color(variant):
    # check for disease
    if is_in_list(variant["clinicalSignificances"], variation_consequences["likely_disease"]):
        return variation_scale_colors["up_disease_color"]
    # check for non-disease
    elif is_in_list(variant["clinicalSignificances"], variation_consequences["likely_benign"]):
        return variation_scale_colors["up_non_disease_color"]
    # check for uncertain, set as others color
    elif (variant["clinicalSignificances"] is None and variant["polyphenScore"] is None and variant["siftScore"] is None) or is_in_list(variant["clinicalSignificances"], variation_consequences["uncertain"]):
        return variation_scale_colors["others_color"]
    # check for prediction
    elif variant["polyphenScore"] != None or variant["siftScore"] != None:
        return get_variation_prediction_color(variant["polyphenScore"], variant["siftScore"])
    # else, set as others color
    else:
        return variation_scale_colors["others_color"]

def is_in_list(search_term, search_list):
    """
    This utility searches if there is an item in list 1 present in list 2. This always check ignoring cases.

    Arguments:
        1) search_term - Can be a str or list. Converted to list if str is passed. This is the input list
        2) search_list - This is the list where each item of search_term is being checked

    Returns:
        True if atleast an item is common in both lists, False otherwise
    """

    dict_search = {}

    if search_term is None:
        return False

    # convert to a list, if not
    if not isinstance(search_term, list):
        search_term = [search_term]
    
    for x in search_term:
        dict_search[x.lower()] = True

    for item in search_list:
        if dict_search.get(item.lower()) is not None:
            return True

    return False

def get_source_type(xrefs, source_type):
    return_list = list(map(lambda x: x["name"], xrefs))
    
    if source_type in ["uniprot", "mixed"]:
        return_list.append("uniprot")

    return return_list


def format_variation_tooltip(variant):

    descriptions = {}
    if variant.get("description"):
        temp_list = variant["description"].split("[LSS_")

        # check if an LSS description, add to LSS dictionary
        if len(temp_list) > 1:
            descriptions["LSS"] = ";".join(temp_list[1:]).replace("]:", ":")

        temp_list = variant["description"].split("[SWP")

        # check if a UP description, add to UP dictionary
        if len(temp_list) > 1:
            descriptions["UP"] = ";".join(temp_list[1:]).replace("]: ", "")

    frequency_tooltip = ""
    if variant.get("frequency"):
        frequency_tooltip = f"""
        <tr>
            <td>Frequency (MAF)</td>
            <td>{variant["frequency"]}</td>
        </tr>
        """

    sift_tooltip = ""
    if variant.get("siftScore"):
        sift_tooltip = f"""
        <tr>
            <td>SIFT</td>
            <td>{variant["siftScore"]}</td>
        </tr>
        """

    polyphen_score_tooltip = ""
    if variant.get("polyphenScore"):
        polyphen_score_tooltip = f"""
        <tr>
            <td>Polyphen</td>
            <td>{variant["polyphenScore"]}</td>
        </tr>
        """

    consequence_tooltip = ""
    if variant.get("consequenceType"):
        consequence_tooltip = f"""
        <tr>
            <td>Consequence</td>
            <td>{variant["consequenceType"]}</td>
        </tr>
        """

    somatic_tooltip = ""
    if variant.get("somaticStatus"):
        somatic_tooltip = f"""
        <tr>
            <td>Somatic</td>
            <td>{"No" if variant["somaticStatus"] == 0 else "Yes"}</td>
        </tr>
        """

    location_tooltip = ""
    if variant.get("genomicLocation"):
        genomic_location = variant["genomicLocation"]

        # check for missense - PDBE-3712: Removed location for missense annotations
        # check for foldx - PDBE-3956: Removed location for foldx annotations
        if not variant.get("clinicalSignificances") in ["missense", "foldx", "skempi"]:
            # genomic_location = f"""{variant.get("wildType")}>{variant.get("alternativeSequence")}"""

            location_tooltip = f"""
            <tr>
                <td>Location</td>
                <td>{genomic_location}</td>
            </tr>
            """
    
    category_tooltip = ""
    if variant.get("category"):
        category_tooltip = f"""
        <tr>
            <td>Category</td>
            <td>{variant.get("category")}</td>
        </tr>
        """

    resource_url_tooltip = ""
    
    if variant.get("resourceUrl"):
        clinical_significances = variant.get("clinicalSignificances")
        resource_type = "Missense3D"

        if clinical_significances == "foldx":
            resource_type = "FoldX"
        elif clinical_significances == "skempi":
            resource_type = "SKEMPI"
        elif clinical_significances == "fireprotdb":
            resource_type = "FireProt DB"
        elif clinical_significances == "frustratometer":
            resource_type = "Frustratometer"
        elif clinical_significances == "nextprot":
            resource_type = "neXtProt"

        resource_url_tooltip = f"""
        <tr>
            <td>Source</td>
            <td><a href="{variant.get("resourceUrl")}" target="_blank">{resource_type}</a></td>
        </tr>
        """
    
    raw_score_tooltip = ""
    raw_score_unit = ""
    if variant.get("rawScore"):
        clinical_significances = variant.get("clinicalSignificances")
        
        if clinical_significances == "fireprotdb":
            raw_score_unit = "(ddG)"
            
        raw_score_tooltip = f"""
        <tr>
            <td>Raw score {raw_score_unit}</td>
            <td>{variant.get("rawScore")}</td>
        </tr>
        """

    return f"""
        <table>
            <tr>
                <td>Variant</td>
                <td>{variant["wildType"]} > {variant["alternativeSequence"]}</td>
            </tr>
            {frequency_tooltip}
            {sift_tooltip}
            {polyphen_score_tooltip}
            {category_tooltip}
            {consequence_tooltip}
            {resource_url_tooltip}
            {raw_score_tooltip}
            {somatic_tooltip}
            {location_tooltip}
            {get_variation_uniprot_html(descriptions, variant) if variant["sourceType"] in ["uniprot", "mixed"] else ""}
            {get_variation_lss_html(descriptions, variant) if variant["sourceType"] in ["large_scale_study", "mixed"] else ""}
        </table>
    """


def get_variation_uniprot_html(descriptions, variant):
    if descriptions.get("UP"):
        feature = None
        if variant.get("ftId"):
            feature = f"""<tr><td>Feature ID</td><td>{variant["ftId"]}</td></tr>"""

        return f"""
            <tr>
                <td colspan="2"><h4>UniProt</h4></td>
            </tr>
            <tr>
                <td>Description</td>
                <td>{descriptions["UP"]}</td>
            </tr>
            {feature if feature else ""}
            {"" if variant.get("association") is None else get_variation_associations(variant["association"])}
        """
    return ""

def get_variation_lss_html(descriptions, variant):
    if descriptions.get("LSS"):
        frequency = None
        
        if variant.get("frequency"):
            frequency += f"""<tr><td>Frequency (MAF)</td><td>${variant["frequency"]}</td></tr>"""


        return f"""
            <tr><td colspan="2"><h4>Large scale studies</h4></td></tr>
            <tr>
                <td>Description</td>
                <td>{descriptions["LSS"]}</td>
            </tr>
            {frequency if frequency else ""}
            <tr><td>Cross-references</td><td>{get_variation_xrefs(variant["xrefs"])}</td></tr>
        """
    return ""

def get_variation_associations(associations):
    result = ""
    for association in associations:
        result += f"""<tr><td>Disease</td><td>${association["name"]}</td></tr>"""

        if association.get("xrefs"):
            result += f"""<tr><td>Cross-references</td><td>{get_variation_xrefs(association["xrefs"])}</td></tr>"""
        if association.get("evidences"):
            result += get_variation_evidences(association["evidences"])

    return result

def get_variation_xrefs(xrefs):
    return "<br>".join(list(map(lambda x: f"""<a href="{x.get("url")}">{x.get("id")}</a> ({x.get("name")})""", xrefs)))

def get_variation_evidences(evidences):
    return "".join(list(map(lambda x: f"""
        <tr><td>Evidence</td><td>{x["code"]}</td></tr>
        <tr><td>Source</td><td><a href="{x["source"].get("url")}">{x["source"].get("id")}</a> ({x["source"].get("name")})</td></tr>
    """, evidences)))

def get_variation_prediction_color(polyphen_score, sift_score):
    final_value = (sift_score if sift_score != 0 else 0 + (polyphen_score if 1 - polyphen_score != 0 else 1))/(2 if polyphen_score != 0 and sift_score != 0 else 1)
    return PREDICTION_GRADIENT[int(final_value * 10)]


def get_variation_filter_keywords(variant_fragment):

    keywords = set()

    # Checking consequences

    # check for likely disease
    if is_in_list(variant_fragment["clinicalSignificances"], variation_consequences["likely_disease"]):
        keywords.add("likely_disease")
    # check for non-disease
    elif is_in_list(variant_fragment["clinicalSignificances"], variation_consequences["likely_benign"]):
        keywords.add("likely_benign")
    # check for uncertain, set as others color
    elif (variant_fragment["clinicalSignificances"] is None and variant_fragment["polyphenScore"] is None and variant_fragment["siftScore"] is None) or is_in_list(variant_fragment["clinicalSignificances"], variation_consequences["uncertain"]):
        keywords.add("uncertain")
    # check for prediction
    elif variant_fragment["polyphenScore"] != None or variant_fragment["siftScore"] != None:
        keywords.add("predicted")
    # check for missense 3d annotations
    elif variant_fragment["clinicalSignificances"] == "missense":
        keywords.add("funpdbe")
        keywords.add("missense3d")
        keywords.add("predicted")
    # check for foldx annotations
    elif variant_fragment["clinicalSignificances"] == "foldx":
        keywords.add("funpdbe")
        keywords.add("foldx")
        keywords.add("predicted")
    # check for skempi annotations
    elif variant_fragment["clinicalSignificances"] == "skempi":
        keywords.add("funpdbe")
        keywords.add("skempi")
        keywords.add("predicted")
    # check for FireProt DB annotations
    elif variant_fragment["clinicalSignificances"] == "fireprotdb":
        keywords.add("funpdbe")
        keywords.add("fireprotdb")
        keywords.add("predicted")
    # check for frustratometer annotations
    elif variant_fragment["clinicalSignificances"] == "frustratometer":
        keywords.add("funpdbe")
        keywords.add("frustratometer")
        keywords.add("predicted")
    # check for nextprot annotations
    elif variant_fragment["clinicalSignificances"] == "nextprot":
        keywords.add("funpdbe")
        keywords.add("nextprot")
        keywords.add("predicted")

    # Checking provenance
    xrefNames = variant_fragment.get("xrefNames")
    sourceType = variant_fragment.get("sourceType")

    # check for uniprot review
    if is_in_list(xrefNames, ["uniprot"]):
        keywords.add("uniprot_reviewed")

    # check for ClinVar review
    if is_in_list(xrefNames, ["ClinVar"]):
        keywords.add("clinvar_reviewed")

    # check for LSS
    if sourceType is not None and sourceType in ["large_scale_study", "mixed"]:
        keywords.add("large_scale_studies")

    return list(keywords)



def scale_color(x: float, zero: RGB = RGB(255, 255, 255), one: RGB = RGB(0, 0, 0)) -> RGB:
    '''Convert a number x in [0, 1] to a color from a heatmap (0 -> zero, 1 -> one).'''
    r = (1-x) * zero.r + x * one.r 
    g = (1-x) * zero.g + x * one.g 
    b = (1-x) * zero.b + x * one.b
    return RGB(r, g, b)


def group_residues(residues: List, aggregate_item):
    """
        Group same residues in a list of residues and aggregates based on the aggregate_item
        using , as a separator

    l = [
        {
            "startIndex": 10,
            "annotations": {
                "label": "some",
            }
        },
        {
            "startIndex": 10,
            "annotations": {
                "label": "other",
            }
        }
    ]
    
    group_residues(l, "annotations.label")
    
    [
        {
            "startIndex": 10,
            "annotations": {
                "label": "some,other",
            }
        },
    ]
    
    
    Args:
        residues (List): List of residues
    """
    residue_dict = Prodict()
    for residue in copy.deepcopy(residues):
        residue = Prodict.from_dict(residue)
        residue_id = residue.startIndex
        if residue_id not in residue_dict:
            residue_dict[residue_id] = residue
        else:
            label = eval(f"residue.{aggregate_item}")
            current_label = eval(f"residue_dict[residue_id].{aggregate_item}")
            eval(f"""residue_dict[residue_id].{".".join(aggregate_item.split(".")[:-1])}""").update(
                {aggregate_item.split(".")[-1]: f"{current_label}, {label}"}
            )
    return list(residue_dict.values())
