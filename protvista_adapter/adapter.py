from protvista_adapter import protein_page, pdbe_page

def convert_to_protvista(target, type_of_call, generic_response):
    """
        Summary:
            Invokes converter depending on the parameters provided
        Args:
            target (str) - This is the target page where the protvista is used. for eg. protein, entry etc. If it is used in protein pages, then provide value as 'protein'
                Allowed values
                1) protein - Specifies protvista used on protein pages
                2) entry - Specifies protvista used on entry pages
            type_of_call (str) - This is the actual call to be invoked and converted
                Allowed values
                1) interface - Corresponds to interface residues call
                2) ligandsites - Corresponds to ligand sites call
                3) domains - Corresponds to sequence and structural domains call
                4) unipdb - Corresponds to unipdb call
                5) annotations - Corresponds to FunPDBe annotations call
                6) secstructures - Corresponds to secondary structures call
                7) variation - Corresponds to variation call
                8) rfam - Corresponds to rfam call
            generic_response (obj) - A generic response object
        Returns:
            A protvista response object as determined by parameters supplied
    """

    # validate inputs
    valid_targets = ["protein", "entry"]
    if target not in valid_targets:
        raise Exception(f"Invalid input passed to target parameter. Allowed values: {valid_targets}")

    valid_call_types = ["interface", "ligandsites", "domains", "unipdb", "annotations", "secstructures", "uniprot", "chains", "variation", "rfam", "processed_protein", "secstructures_var", "flex_predictions"]
    if type_of_call not in valid_call_types:
        raise Exception(f"Invalid input passed to type_of_call parameter. Allowed values: {valid_call_types}")

    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input passed to generic_response.")

    if target == "protein":
        if type_of_call == "interface":
            return protein_page.convert_interface_residues(generic_response)
        elif type_of_call == "ligandsites":
            return protein_page.convert_ligand_residues(generic_response)
        elif type_of_call == "domains":
            return protein_page.convert_domains(generic_response)
        elif type_of_call == "unipdb":
            return protein_page.convert_unipdb(generic_response)
        elif type_of_call == "secstructures":
            return protein_page.convert_secondary_structures(generic_response)
        elif type_of_call == "variation":
            return protein_page.convert_variation(generic_response)
        elif type_of_call == "processed_protein":
            return protein_page.convert_processed_protein(generic_response)
        elif type_of_call == "secstructures_var":
            return protein_page.convert_secondary_structures_variation(generic_response)
        elif type_of_call == "flex_predictions":
            return protein_page.convert_flex_predictions(generic_response)
        else:
            return protein_page.convert_annotations(generic_response)

    elif target == "entry":
        if type_of_call == "uniprot":
            return pdbe_page.convert_uniprot_mapping(generic_response)
        elif type_of_call == "secstructures":
            return pdbe_page.convert_secondary_structures(generic_response)
        elif type_of_call == "ligandsites":
            return pdbe_page.convert_ligand_residues(generic_response)
        elif type_of_call == "interface":
            return pdbe_page.convert_interface_residues(generic_response)
        elif type_of_call == "domains":
            return pdbe_page.convert_domains(generic_response)
        elif type_of_call == "chains":
            return pdbe_page.convert_chains(generic_response)
        elif type_of_call == "variation":
            return pdbe_page.convert_variation(generic_response)
        elif type_of_call == "rfam":
            return pdbe_page.convert_rfam(generic_response)
        else:
            return pdbe_page.convert_annotations(generic_response)
        