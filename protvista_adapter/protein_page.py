from protvista_adapter.common_params import color_protvista_interaction_interfaces, color_protvista_ligand_sites
from protvista_adapter.common_params import funpdbe_resource_dict, funpdbe_resource_type_dict, domains_dict
from protvista_adapter.common_params import color_protvista_predicted_annotations, color_protvista_curated_annotations, domains_color_palette
from protvista_adapter.common_params import common_color_codes, color_protvista_interaction_interfaces_shades, color_residue_conflicts, color_protvista_processed_proteins, color_codes, sec_structure_display_order
from collections import OrderedDict
from protvista_adapter.util_common import get_variation_color, get_source_type, format_variation_tooltip, get_variation_filter_keywords, group_residues
from protvista_adapter.util_common import assign_residue_colors_for_annotations, scale_color
import copy

def convert_interface_residues(generic_response):
    """
        Summary:
            Converts generic interface residues response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic interface residues response
        Returns:
            A protvista specific interface residues response object 
    """
    
    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")

    specific_response = {}
    
    for accession in generic_response.keys():
        accession_data = generic_response[accession]
        is_processed_protein = True if accession.startswith("PRO_") else False
        processed_protein_start = None if not is_processed_protein else accession_data["processed_protein_start"]
        
        specific_response[accession] = {
            "largeLabels": True,
            "sequence": accession_data["sequence"],
            "length": accession_data["length"],
            "tracks": [{
                "labelType": "text",
                "label": "Interaction interfaces",
                "data": []
            }]
        }

        for partner in accession_data["data"]:
            partner_accession = partner["accession"]
            partner_desc = label = partner["name"]
            all_pdb_entries = partner["allPDBEntries"]
            pdb_entities = set()
            is_self = False
            is_prd = partner_accession.startswith("PRD_")
            is_antibody = (partner_accession == 'Antibody')

            if partner_accession and accession == partner_accession: # JIRA PDBE-2400
                label += " (self)"
                is_self = True

            if partner_accession != "Other":
                label += f" ({partner_accession})"

            partner_fragment = {
                "accession": f"{label}",
                "labelType": "text",
                "label": f"{label}",
                "labelTooltip": f"Residues interacting with {label}", # JIRA PDBE-2347
                "color": color_protvista_interaction_interfaces,
                "type": "UniProt range",
                "tooltipContent": "Interaction interfaces",
                "locations": [{
                    "fragments": []
                }],
                "pdb_entities": []
            }

            if is_processed_protein:
                partner_fragment.update({
                    "type": "Range"
                })

            # handle cases for PRD
            if is_prd:
                # get type and class
                partner_accession = f'{partner["additionalData"]["prdType"]} {partner["additionalData"]["prdClass"]} ({partner_accession})'
                partner_fragment["accession"] = partner_fragment["label"] = partner_accession
                partner_fragment["labelTooltip"] = f"Residues interacting with {partner_desc}"

            # handle antibody
            if is_antibody:
                partner_fragment["accession"] = partner_fragment["label"] = partner_accession
                partner_fragment["labelTooltip"] = f"Residues interacting with {partner_desc}"

            if is_self:
                partner_fragment.update({
                        "labelColor": "rgb(173,216,226)"
                })

            for residue in partner["residues"]:
                start_index = residue["startIndex"]
                end_index = residue["endIndex"]
                start_code = residue["startCode"]
                end_code = residue["endCode"]
                tooltip_start_index = start_index
                tooltip_end_index = end_index
                
                if is_processed_protein:
                    tooltip_start_index += (processed_protein_start - 1)
                    tooltip_end_index += (processed_protein_start - 1)

                residue_fragment = {
                    "start": start_index,
                    "end": end_index
                }
                
                pdb_entries = set()

                for pdb_entry in residue["interactingPDBEntries"]:
                    pdb_entries.add(pdb_entry["pdbId"])
                    pdb_entities.add("{}_{}".format(pdb_entry["pdbId"], pdb_entry["entityId"]))
                
                number_all_pdb_entries = len(residue["allPDBEntries"])

                if number_all_pdb_entries != 0:
                    shade_index = int((len(pdb_entries)/number_all_pdb_entries) * 10)
                else:
                    shade_index = len(color_protvista_interaction_interfaces_shades)-1

                residue_fragment.update({
                    "color": color_protvista_interaction_interfaces_shades[shade_index]
                })
                tooltip_range_label = "Range"

                if is_processed_protein:
                    tooltip_range_label = "UniProt range"

                entry_label = "entry" if len(pdb_entries) == 1 else "entries"
                residue_fragment["tooltipContent"] = f"Type: Interface<br>{tooltip_range_label}: {start_code}{tooltip_start_index} - {end_code}{tooltip_end_index}<br>Partner: {partner_desc}<br>Observed in {len(pdb_entries)} {entry_label}, e.g. <a target=\"_blank\" href=\"https://www.ebi.ac.uk/pdbe/entry/pdb/{list(pdb_entries)[0]}\">{list(pdb_entries)[0]}</a>"
                
                partner_fragment["locations"][0]["fragments"].append(residue_fragment)

            partner_fragment["pdb_entities"] = list(pdb_entities)
            specific_response[accession]["tracks"][0]["data"].append(partner_fragment)
        
    return specific_response
    

def convert_ligand_residues(generic_response):
    """
        Summary:
            Converts generic ligand binding residues response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic ligand binding residues response
        Returns:
            A protvista specific ligand binding residues response object 
    """
    
    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")

    specific_response = {}
    
    for accession in generic_response.keys():
        accession_data = generic_response[accession]
        is_processed_protein = True if accession.startswith("PRO_") else False
        processed_protein_start = None if not is_processed_protein else accession_data["processed_protein_start"]

        specific_response[accession] = {
            "largeLabels": True,
            "sequence": accession_data["sequence"],
            "length": accession_data["length"],
            "tracks": [{
                "labelType": "text",
                "label": "Ligand binding sites",
                "data": []
            }]
        }

        for chem_comp_element in accession_data["data"]:
            chem_comp = chem_comp_element["accession"]
            chem_comp_desc = label = chem_comp_element["name"]
            all_pdb_entries = chem_comp_element["additionalData"]["pdbEntries"]
            co_factor_id = chem_comp_element["additionalData"]["coFactorId"]
            drugbank_id = chem_comp_element["additionalData"]["drugBankId"]
            chembl_id = chem_comp_element["additionalData"]["chemblId"]
            rxn_id = chem_comp_element["additionalData"]["reactionId"]

            partner_fragment = {
                "labelType": "text",
                "accession": chem_comp,
                "label": chem_comp,
                "color": color_protvista_ligand_sites,
                "type": "UniProt range",
                "tooltipContent": "Ligand binding sites",
                "labelTooltip": f"Residues binding {chem_comp_desc}<br><span style=\"display:inline-block;width:100%;text-align:center;margin-top:5px;\"><img src=\"https://www.ebi.ac.uk/pdbe/static/files/pdbechem_v2/{chem_comp}_200.svg\" /><span>",
                "scaffold_id": chem_comp_element["additionalData"]["scaffoldId"],
                "cofactor_id": co_factor_id,
                "residue_count": len(chem_comp_element["residues"]),
                "locations": [{
                    "fragments": []
                }]
            }

            if is_processed_protein:
                partner_fragment.update({
                    "type": "Range"
                })

            annotations = []

            if co_factor_id != "":
                annotations.append("cofactor")

            if drugbank_id != "":
                annotations.append("drug")
            
            if chembl_id != "" and rxn_id !="":
                annotations.append("reactant")

            annotations_label = "" if not annotations else f" ({'/'.join(annotations)}-like)"

            partner_fragment.update({
                    "labelColor": "rgb(239,247,232)",
                    "label": f"{chem_comp}{annotations_label}"
                })

            for residue in chem_comp_element["residues"]:
                start_index = residue["startIndex"]
                end_index = residue["endIndex"]
                start_code = residue["startCode"]
                end_code = residue["endCode"]
                tooltip_start_index = start_index
                tooltip_end_index = end_index
                
                if is_processed_protein:
                    tooltip_start_index += (processed_protein_start - 1)
                    tooltip_end_index += (processed_protein_start - 1)

                residue_fragment = {
                    "start": start_index,
                    "end": end_index
                }

                interacting_pdb_entries = set()

                for interacting_pdb_entry in residue["interactingPDBEntries"]:
                    interacting_pdb_entries.add(interacting_pdb_entry["pdbId"])

                number_mapped_pdb_entries = len(residue["allPDBEntries"])
                entry_label = "entry" if len(interacting_pdb_entries) == 1 else "entries"

                tooltip_range_label = "Range"

                if is_processed_protein:
                    tooltip_range_label = "UniProt range"

                residue_fragment["tooltipContent"] = f"Type: Ligand binding site<br>{tooltip_range_label}: {start_code}{tooltip_start_index} - {end_code}{tooltip_end_index}<br>Ligand: {chem_comp}<br>Observed in {len(interacting_pdb_entries)} {entry_label}, e.g. <a target=\"_blank\" href=\"https://www.ebi.ac.uk/pdbe/entry/pdb/{list(interacting_pdb_entries)[0]}\">{list(interacting_pdb_entries)[0]}</a>"
                
                residue_fragment.update({
                    "color": color_protvista_interaction_interfaces_shades[int((len(interacting_pdb_entries)/number_mapped_pdb_entries) * 10)]
                })

                partner_fragment["locations"][0]["fragments"].append(residue_fragment)

            specific_response[accession]["tracks"][0]["data"].append(partner_fragment)

    return specific_response


def convert_annotations(generic_response):
    """
        Summary:
            Converts generic annotations response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic annotations response
        Returns:
            A protvista specific annotations response object 
    """

    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")
    
    specific_response = {}

    for accession in generic_response.keys():
        accession_data = generic_response[accession]
        is_processed_protein = True if accession.startswith("PRO_") else False
        processed_protein_start = None if not is_processed_protein else accession_data["processed_protein_start"]

        dict_resource_type = dict()

        specific_response[accession] = {
            "largeLabels": True,
            "sequence": accession_data["sequence"],
            "length": accession_data["length"],
            "tracks": [],
            "legends": {
                "alignment": "right",
                "data": {
                    "Annotations": []
                }
            }
        }

        for resource in accession_data["data"]:
            resource_name = resource["name"]
            resource_types = funpdbe_resource_dict[resource_name]["label"]
            accession_label = resource_name

            # skip ChannelsDB now
            # if resource_name == "ChannelsDB":
            #     continue

            if accession_label == "3Dcomplex":
                accession_label = "3DComplex"

            # Some resources can have Curated or Predicted category. eg. ChannelsDB
            if not type(resource_types) == list:
                resource_types = [resource_types]
            
            for resource_type in resource_types:
                # set dictionary of type of resource
                if dict_resource_type.get(resource_type) is None:
                    dict_resource_type[resource_type] = {
                        "labelType": "text",
                        "label": resource_type,
                        "data": []
                    }

                    if resource_type.startswith("Predicted") or resource_type == "Biophysical parameters" or resource_type == "Molecular channels": # PDBE-2399, PDBE-3714
                        dict_resource_type[resource_type].update({"labelColor": "rgb(128,128,128)"})
                
                resource_fragment = {
                    "accession": accession_label,
                    "labelType": "text",
                    "label": "<a target=\"_blank\" href=\"{}\">{} <i class=\"icon icon-generic\" style=\"font-size:75%\" data-icon=\"x\"></i></a>".format(funpdbe_resource_dict[resource_name]["link"], funpdbe_resource_dict[resource_name]["displayName"]),
                    "color": funpdbe_resource_dict[resource_name]["color"],
                    "locations": [{
                        "fragments": []
                    }],
                    "type": "UniProt range", # fix for PDBE-2297, changed resource name to static text 'UniProt range'
                    "tooltipContent": funpdbe_resource_dict[resource_name]["displayName"],
                    "labelTooltip": funpdbe_resource_dict[resource_name]["desc"]
                }

                tooltip_range_label = "Range"

                if is_processed_protein:
                    resource_fragment.update({
                        "type": "Range"
                    })
                    tooltip_range_label = "UniProt range"
                
                if resource_type.startswith("Predicted") or resource_type == "Biophysical parameters" or resource_type == "Molecular channels": # PDBE-2399, PDBE-3714
                    resource_fragment.update({"labelColor": "rgb(211,211,211)"})
                
                tooltip_resource_type = resource_type
                tooltip_source_name = funpdbe_resource_dict[resource_name]["displayName"]

                # PDBE-2880 - change type for canSAR
                if resource_name == "canSAR":
                    tooltip_resource_type = "Predicted druggable pockets"
                elif resource_name in ["Total SASA", "Hydrophobic SASA", "Hydrophilic SASA"]:
                    tooltip_resource_type = "Solvent accessible surface area"
                    tooltip_source_name = "POPScomp"
                elif resource_name in ["ASA alone", "ASA in assembly"]:
                    tooltip_resource_type = "Solvent accessible surface area"
                    tooltip_source_name = "3DComplex"
                    tooltip_range_label = "PDB residue"

                colored_residues = resource["residues"]
                
                # assign colors only if enabled in configuration
                if funpdbe_resource_dict[resource_name]["assign_color"]:
                    colored_residues = assign_residue_colors_for_annotations(resource["residues"], funpdbe_resource_dict[resource_name]["color_gradient"])

                for residue in colored_residues:
                    pdbs = list(map(lambda x: x["pdbId"], residue["pdbEntries"]))
                    pdbs = list(set(pdbs))
                    entry_label = "1 entry" if len(pdbs) == 1 else "{} entries".format(len(pdbs))
                    resource_url = residue["pdbEntries"][0]["additionalData"]["resourceUrl"]
                    raw_score = residue["pdbEntries"][0]["additionalData"]["rawScore"]
                    raw_score_label = f"Raw score: {raw_score}<br>" if raw_score else ""
                    group_label = residue["pdbEntries"][0]["additionalData"]["groupLabel"]
                    conf_level = residue["pdbEntries"][0]["additionalData"]["confLevel"]
                    conf_level_label = f"Confidence level: {conf_level}<br>" if resource_name in ["ChannelsDB", "Scop3P"] else ""
                    conf_score = residue["pdbEntries"][0]["additionalData"]["confScore"]
                    conf_score_label = f"Confidence score: {conf_score}<br>" if resource_name == "Scop3P" and conf_score else ""

                    # use resource url if resource entry url is blank: PDBE-2539
                    if resource_url == '':
                        resource_url = funpdbe_resource_dict[resource_name]["link"]
                    
                    start_index = residue["startIndex"]
                    end_index = residue["endIndex"]
                    tooltip_start_index = start_index
                    tooltip_end_index = end_index
                    residue_color = residue.get("color")

                    if is_processed_protein:
                        tooltip_start_index += (processed_protein_start - 1)
                        tooltip_end_index += (processed_protein_start - 1)

                    tooltip_range = "{}{} - {}{}".format(residue["startCode"], tooltip_start_index, residue["endCode"], tooltip_end_index)

                    if resource_name in ["ASA alone", "ASA in assembly"]:
                        tooltip_range = "{}{}".format(residue["startCode"], tooltip_start_index)

                    residue_fragment = {
                        "start": start_index,
                        "end": end_index,
                        "tooltipContent": "Type: {}<br>Label: {}<br>{}: {}<br>Observed in {}, e.g. <a target=\"_blank\" href=\"https://www.ebi.ac.uk/pdbe/entry/pdb/{}\">"
                            "{}</a><br>Source: <a target=\"_blank\" href=\"{}\">{}</a><br>{}{}{}".format(tooltip_resource_type, group_label, tooltip_range_label, tooltip_range, entry_label, pdbs[0], pdbs[0], resource_url, tooltip_source_name, raw_score_label, conf_level_label, conf_score_label),
                        "pdbs": pdbs
                    }

                    if residue_color:
                        residue_fragment.update({
                            "color": residue_color
                        })
                    
                    resource_fragment["locations"][0]["fragments"].append(residue_fragment)

                resource_fragment["locations"][0]["fragments"] = sorted(resource_fragment["locations"][0]["fragments"], key=lambda x: x["start"])
                dict_resource_type[resource_type]["data"].append(resource_fragment)
            

        predicted_legend_added = False
        curated_legend_added = False

        for resource_type in dict_resource_type.keys():
            
            # set flags appropriately for legends
            if resource_type.startswith("Predicted"):
                predicted_legend_added = True
            elif resource_type.startswith("Curated"):
                curated_legend_added = True
            
            specific_response[accession]["tracks"].append(dict_resource_type[resource_type])

        specific_response[accession]["tracks"] = sorted(specific_response[accession]["tracks"], key=lambda x: funpdbe_resource_type_dict[x["label"]][1])

        # add legends
        if predicted_legend_added:
            specific_response[accession]["legends"]["data"]["Annotations"].append({
                "color": color_protvista_predicted_annotations,
                "text": "Predicted annotations"
            })
        if curated_legend_added:
            specific_response[accession]["legends"]["data"]["Annotations"].append({
                "color": color_protvista_curated_annotations,
                "text": "Curated annotations"
            })
        

    return specific_response


def convert_domains(generic_response):
    """
        Summary:
            Converts generic sequence and structural domains response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic sequence and structural domains response
        Returns:
            A protvista specific sequence and structural domains response object 
    """

    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")
    
    specific_response = {}

    for accession in generic_response.keys():
        accession_data = generic_response[accession]
        domains_master_dict = dict()
        is_processed_protein = True if accession.startswith("PRO_") else False
        processed_protein_start = None if not is_processed_protein else accession_data["processed_protein_start"]

        # flags for domain legends
        legend_dict = {
            "Pfam": False,
            "CATH": False,
            "CATH-B": False,
            "SCOP": False,
            "InterPro": False
        }

        specific_response[accession] = {
            "largeLabels": True,
            "sequence": accession_data["sequence"],
            "length": accession_data["length"],
            "tracks": [{
                "labelType": "text",
                "label": "Domains",
                "data": []
            }],
            "legends": {
                "alignment": "right",
                "data": {
                    "Domains": []
                }
            }
        }

        domain_color_index = dict()
        domain_color = dict()

        for domain in accession_data["data"]:
            domain_type = domain["dataType"]
            
            if domain_color_index.get(domain_type) is None:
                domain_color_index[domain_type] = 0
            
            # reset index if exceeded maximum colors
            if domain_color_index[domain_type] == len(domains_color_palette[domain_type]):
                domain_color_index[domain_type] = 0

            # set True in legend dictionary if type of domain is present
            legend_dict[domain_type] = True

            if domains_master_dict.get(domain_type) is None:
                domains_master_dict[domain_type] = {
                    "accession": f"{domain_type} domains",
                    "labelType": "text",
                    "label": f"{domain_type} domains",
                    "labelTooltip": f"Residues mapped to {domain_type} domains",
                    "color": domains_dict[domain_type][0],
                    "type": "UniProt range",
                    "locations": [{
                        "fragments": []
                    }]
                }

                if is_processed_protein:
                    domains_master_dict[domain_type].update({
                        "type": "Range"
                    })

                # PDBE-2694
                if domain_type == "InterPro":
                    domains_master_dict[domain_type].update({
                        "accession": f"{domain_type} annotations",
                        "label": f"{domain_type} annotations",
                    })

            site_url = domains_dict[domain_type][1] +domain["accession"]

            # # add accession to site url except for SCOP
            # if domain_type != "SCOP":
            #     site_url += domain["accession"]

            temp_text = "domain"
            if domain_type == "InterPro":
                temp_text = "annotation"

            tooltip_range_label = "Range"

            if is_processed_protein:
                tooltip_range_label = "UniProt range"

            for residue in domain["residues"]:

                if domain_color.get(domain["accession"]) is None:
                    temp_color = domains_color_palette.get(domain_type)[domain_color_index.get(domain_type)]
                    domain_color[domain["accession"]] = temp_color
                    domain_color_index[domain_type] += 1
                else:
                    temp_color = domain_color[domain["accession"]]

                start_index = residue["startIndex"]
                end_index = residue["endIndex"]
                tooltip_start_index = start_index
                tooltip_end_index = end_index
                
                if is_processed_protein:
                    tooltip_start_index += processed_protein_start
                    tooltip_end_index += (processed_protein_start - 1)

                domains_master_dict[domain_type]["locations"][0]["fragments"].append({
                    "start": start_index,
                    "end": end_index,
                    "tooltipContent": "Type: {} {}<br>{}: {}{} - {}{}<br>Source: <a target=\"_blank\" href=\"{}\">{} ({})</a>".format(domain_type, temp_text,
                        tooltip_range_label, residue["startCode"], tooltip_start_index, residue["endCode"], tooltip_end_index, site_url, domain["name"], domain["accession"]),
                    "color": f"rgb({temp_color})"
                })

        for a, b in domains_master_dict.items():
            specific_response[accession]["tracks"][0]["data"].append(b)

        # add domains to legends depending on availability
        for key in legend_dict.keys():
            if legend_dict[key]:
                legend_fragment = {
                    "color": list(map(lambda x: "rgb({})".format(x), domains_color_palette[key])),
                    "text": f"{key} domains"
                }

                # PDBE-2694
                if key == "InterPro":
                    legend_fragment.update({
                        "text": f"{key} annotations"
                    })

                specific_response[accession]["legends"]["data"]["Domains"].append(legend_fragment)

    return specific_response



def convert_unipdb(generic_response):
    """
        Summary:
            Converts generic unipdb response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic unipdb response
        Returns:
            A protvista specific unipdb response object 
    """

    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")
    
    specific_response = {}

    for accession in generic_response.keys():
        accession_data = generic_response[accession]
        
        specific_response[accession] = {
            "largeLabels": True,
            "sequence": accession_data["sequence"],
            "length": accession_data["length"],
            "tracks": [{
                "labelType": "text",
                "label": "",
                "data": []
            }],
            "legends": {
                "alignment": "right",
                "data": {
                    "Other": [
                        {"color": "rgb(65, 105, 225)", "text": "Observed"}
                    ]
                }
            }
        }
        
        is_processed_protein = True if accession.startswith("PRO_") else False
        processed_protein_start = None if not is_processed_protein else accession_data["processed_protein_start"]
        tooltip_range_label = "Range"

        if is_processed_protein:
            tooltip_range_label = "UniProt range"

        entry_dict = OrderedDict()
        unobserved_legend_to_add = False
        conflict_legend_to_add = False

        for data in accession_data["data"]:
            entry_id = data["accession"]
            ligand_label = 'no' if data["additionalData"].get("ligandCount") is 0 else data["additionalData"].get("ligandCount")
            complex_label = 'not ' if data["additionalData"].get("entityCount") is 1 else ''
            nonPolyEntities = data["additionalData"].get("nonPolyTypes")
            dna_rna_label = ""

            if not nonPolyEntities:
                dna_rna_label = "no "

            if entry_dict.get(entry_id) is None:
                entry_dict[entry_id] = {
                    "accession": entry_id,
                    "labelType": "pdbIcons",
                    "label": {
                        "id": entry_id,
                        "url": f"https://pdbe.org/{entry_id}",
                        "resolution": data["additionalData"]["resolution"],
                        "icons": [
                            {
                                "type": "experiments",
                                "background": "{}".format(common_color_codes["grey"] if not data["additionalData"].get("experiment") else common_color_codes["green"]),
                                "tooltipContent": "This entry has {} data".format(data["additionalData"].get("experiment")),
                                "url": f"http://www.ebi.ac.uk/pdbe/entry/pdb/{entry_id}/experiment"
                            },
                            {
                                "type": "complex",
                                "background": "{}".format(common_color_codes["grey"] if data["additionalData"].get("entityCount") is 1 else common_color_codes["green"]),
                                "tooltipContent": "This entry is a {}complex".format(complex_label),
                                "url": "https://pdbe.org/{}".format(entry_id)
                            },
                            {
                                "type": "nucleicAcids",
                                "background": "{}".format(common_color_codes["grey"] if not nonPolyEntities else common_color_codes["green"]),
                                "tooltipContent": "This entry has {}RNA/DNA".format(dna_rna_label),
                                "url": "http://www.ebi.ac.uk/pdbe/entry/pdb/{}/analysis".format(entry_id)
                            },
                            {
                                "type": "ligands",
                                "background": "{}".format(common_color_codes["grey"] if data["additionalData"].get("ligandCount") is 0 else common_color_codes["green"]),
                                "tooltipContent": "This entry has {} ligand(s)".format(ligand_label),
                                "url": "http://www.ebi.ac.uk/pdbe/entry/pdb/{}/ligands/".format(entry_id)
                            },
                            {
                                "type": "literature",
                                "background": common_color_codes["green"],
                                "tooltipContent": "Go to literature",
                                "url": "http://www.ebi.ac.uk/pdbe/entry/pdb/{}/citations".format(entry_id)
                            }
                        ]
                    },
                    "color": "#4169e1",
                    "locations": [{
                        "fragments": []
                    }],
                    "type": "UniProt range", # JIRA PDBE-2323
                    "labelTooltip": data["additionalData"]["title"],
                    "observed_residue_count": data["additionalData"]["residueCount"]
                }

                if is_processed_protein:
                    entry_dict[entry_id].update({
                        "type": "Range"
                    })

            else:
                entry_dict[entry_id]["observed_residue_count"] += data["additionalData"]["residueCount"]

            for residue in data["residues"]:
                start_index = residue["startIndex"]
                end_index = residue["endIndex"]
                tooltip_start_index = start_index
                tooltip_end_index = end_index
                
                if is_processed_protein:
                    tooltip_start_index += (processed_protein_start - 1)
                    tooltip_end_index += (processed_protein_start - 1)

                residue_fragment = {
                    "start": start_index,
                    "end": end_index,
                    "start_code":  residue["startCode"],
                    "end_code": residue["endCode"],
                    "tooltipContent": "Type: PDB mapped to UniProt<br>{}: {}{}-{}{}<br>PDB entry: <a target=\"_blank\""
                        "href=\"http://www.ebi.ac.uk/pdbe/entry/pdb/{}/protein/{}\">{} Best Chain {}</a>".format(tooltip_range_label, residue["startCode"], tooltip_start_index, residue["endCode"], tooltip_end_index,
                            entry_id, data["entityId"], entry_id, data["bestChainId"])
                }

                if residue.get("observed") is not None and residue.get("observed") == "N":
                    residue_fragment["color"] = "rgb(211, 211, 211)"
                    unobserved_legend_to_add = True
                    residue_fragment["tooltipContent"] += "<br>Unobserved segment"

                # check for mutation and modification
                if residue.get("mutation") is not None and residue.get("mutation") == True:
                    residue_fragment["color"] = color_residue_conflicts
                    mutation_type = residue.get("mutationType")
                    residue_fragment["tooltipContent"] += "<br>Conflict: {} --> {} ({})".format(residue["startCode"], residue["pdbCode"], mutation_type)
                    conflict_legend_to_add = True
                elif residue.get("modification") is not None and residue.get("modification") == True:
                    residue_fragment["color"] = color_residue_conflicts
                    residue_fragment["tooltipContent"] += "<br>Modified residue: {}".format(residue["pdbCode"])
                    conflict_legend_to_add = True

                entry_dict[entry_id]["locations"][0]["fragments"].append(residue_fragment)
            
        for key, value in entry_dict.items():
            value["locations"][0]["fragments"] = sorted(value["locations"][0]["fragments"], key=lambda x: x["start"])
            specific_response[accession]["tracks"][0]["data"].append(value)

        # PDBE-2673: add unobserved to legend only if atleast one unobserved segment exists
        if unobserved_legend_to_add:
            specific_response[accession]["legends"]["data"]["Other"].append({"color": "rgb(211, 211, 211)", "text": "Unobserved"})

        # PDBE-3019: add conflict to legend only if atleast one conflict exists
        if conflict_legend_to_add:
            specific_response[accession]["legends"]["data"]["Other"].append({"color": color_residue_conflicts, "text": "Conflict"})

        # set label to count of PDB structures
        specific_response[accession]["tracks"][0]["label"] = "PDB Structures ({})".format(len(specific_response[accession]["tracks"][0]["data"]))

        # sort the list so that structures with highest number of observed residues comes on top
        # specific_response[accession]["tracks"][0]["data"] = sorted(specific_response[accession]["tracks"][0]["data"], key=lambda x: x["observed_residue_count"], reverse=True)

    return specific_response


def convert_secondary_structures(generic_response):
    """
        Summary:
            Converts generic secondary structures response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic secondary structures response
        Returns:
            A protvista specific secondary structures response object 
    """

    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")
    
    specific_response = {}

    for accession in generic_response.keys():
        accession_data = generic_response[accession]
        master_dict = dict()

        is_processed_protein = True if accession.startswith("PRO_") else False
        processed_protein_start = None if not is_processed_protein else accession_data["processed_protein_start"]
        tooltip_range_label = "Range"

        if is_processed_protein:
            tooltip_range_label = "UniProt range"

        specific_response[accession] = {
            "largeLabels": True,
            "sequence": accession_data["sequence"],
            "length": accession_data["length"],
            "tracks": [],
            "legends": {
                "alignment": "right",
                "data": {
                    "Secondary structure": []
                }
            }
        }

        # flags to set legends
        helix_present = False
        sheet_present = False
        sec_structure_track = {
            "labelType": "text",
            "label": "Secondary structure",
            "data": [],
            "overlapping": "true"
        }

        for data in accession_data["data"]:
            sec_structure_name = sec_structure_accession = data["name"]

            if data["dataType"] == "Helix":
                helix_present = True
            elif data["dataType"] == "Strand":
                sheet_present = True

            if master_dict.get(sec_structure_name) is None:
                master_dict[sec_structure_name] = {
                    "accession": sec_structure_name,
                    "labelType": "text",
                    "label": sec_structure_name,
                    "color": common_color_codes[data["dataType"]],
                    "locations": [{
                        "fragments": []
                    }],
                    "type": data["dataType"],
                    "tooltipContent": data["dataType"],
                    "labelTooltip": "Residues forming {}".format(data["dataType"])
                }
            
            for residue in data["residues"]:
                start_index = residue["startIndex"]
                end_index = residue["endIndex"]
                tooltip_start_index = start_index
                tooltip_end_index = end_index
                
                if is_processed_protein:
                    tooltip_start_index += (processed_protein_start - 1)
                    tooltip_end_index += (processed_protein_start - 1)

                tool_tip_content = "Type: {}<br>{}: {}{} - {}{}<br>Source PDB: <a target=\"_blank\" href=\"https://www.ebi.ac.uk/pdbe/entry/pdb/{}\">{}</a>".format(data["dataType"],
                        tooltip_range_label, residue["startCode"], tooltip_start_index, residue["endCode"], tooltip_end_index, residue["pdbEntries"][0]["pdbId"], residue["pdbEntries"][0]["pdbId"])
                
                fragment = {
                    "start": start_index,
                    "end": end_index,
                    "pdb_start": residue["pdbStartIndex"],
                    "pdb_end": residue["pdbEndIndex"],
                    "tooltipContent": tool_tip_content
                }

                master_dict[sec_structure_name]["locations"][0]["fragments"].append(fragment)


        for data_type, values in master_dict.items():
            values["locations"][0]["fragments"] = sorted(values["locations"][0]["fragments"], key=lambda x: x["start"])
            sec_structure_track["data"].append(values)

        # PDBE-2673: add legends only if exists
        # add legends
        if helix_present:
            specific_response[accession]["legends"]["data"]["Secondary structure"].append({
                "text": "Helix",
                "color": common_color_codes["Helix"]
            })
        
        # PDBE-2673: add legends only if exists
        if sheet_present:
            specific_response[accession]["legends"]["data"]["Secondary structure"].append({
                "text": "Strand",
                "color": common_color_codes["Strand"]
            })
        
        if helix_present or sheet_present:
            specific_response[accession]["tracks"].append(sec_structure_track)

    return specific_response


def convert_flex_predictions(generic_response):
    """
        Summary:
            Converts generic secondary structures response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic secondary structures response
        Returns:
            A protvista specific secondary structures response object 
    """

    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")
    
    specific_response = {}

    for accession in generic_response.keys():
        accession_data = generic_response[accession]
        master_dict = dict()

        is_processed_protein = True if accession.startswith("PRO_") else False
        processed_protein_start = None if not is_processed_protein else accession_data["processed_protein_start"]
        tooltip_range_label = "Range"

        if is_processed_protein:
            tooltip_range_label = "UniProt range"

        specific_response[accession] = {
            "largeLabels": True,
            "sequence": accession_data["sequence"],
            "length": accession_data["length"],
            "tracks": [],
            "legends": {
                "alignment": "right",
                "data": {
                    "Flexibility predictions": [],
                    "Early folding residue predictions": [],
                }
            }
        }

        # flags to set legends
        idp_present = False
        knotprot_present = False
        webnma_present = False
        kincore_present = False
        dynamine_present = False
        efold_present = False
        idp_fragments = []
        topology_fragments = []
        efold_fragments = []

        for data in accession_data["data"]:
            sec_structure_name = sec_structure_accession = data["name"]

            if data["dataType"] == "MobiDB":
                idp_present = True
            elif data["dataType"] == "KnotProt":
                knotprot_present = True
            elif data["dataType"] == "WEBnma":
                webnma_present = True
            elif data["dataType"] == "KinCore":
                kincore_present = True
            elif data["dataType"] == "dynamine":
                dynamine_present = True
            elif data["dataType"] == "efoldmine":
                efold_present = True
                
            # PDBE-4660 - assign colors to WEBnma residues
            if data["dataType"] == "WEBnma":
                data["residues"] = assign_residue_colors_for_annotations(data["residues"], None)
            elif data["dataType"] in ["dynamine", "efoldmine"]:
                data["residues"] = assign_residue_colors_for_annotations(data["residues"], None, inverted=True)
            elif data["dataType"] == "KinCore":
                data["residues"] = group_residues(data["residues"], "additionalData.groupLabel")

            dict_key = (sec_structure_name, sec_structure_name)
            
            if master_dict.get(dict_key) is None:
                master_dict[dict_key] = {
                    "accession": sec_structure_name,
                    "labelType": "text",
                    "label": sec_structure_name,
                    "color": common_color_codes[data["dataType"]],
                    "locations": [{
                        "fragments": []
                    }],
                    "type": data["dataType"],
                    "tooltipContent": data["dataType"],
                    "labelTooltip": "Residues forming {}".format(data["dataType"])
                }

                if sec_structure_name in ["MobiDB", "WEBnma", "dynamine", "efoldmine"]:
                    master_dict[dict_key].update({"labelColor": "rgb(211,211,211)"})
                    
                if sec_structure_name == "dynamine":
                    master_dict[dict_key].update({"label": "DynaMine"})
                elif sec_structure_name == "efoldmine":
                    master_dict[dict_key].update({"label": "EFoldMine"})
                elif sec_structure_name == "KinCore":
                    master_dict[dict_key].update({"labelTooltip": "Topology annotation from KinCore"})

            for residue in data["residues"]:                
                start_index = residue["startIndex"]
                end_index = residue["endIndex"]
                tooltip_start_index = start_index
                tooltip_end_index = end_index
                tool_tip_content = None
                
                if is_processed_protein:
                    tooltip_start_index += (processed_protein_start - 1)
                    tooltip_end_index += (processed_protein_start - 1)

                if data["dataType"] == "MobiDB":
                    tool_tip_content = "Type: Consensus Disorder Prediction<br>{}: {}{} - {}{}<br>Source : <a target=\"_blank\" href=\"http://mobidb.bio.unipd.it/{}\">MobiDB</a>".format(
                        tooltip_range_label, residue["startCode"], tooltip_start_index, residue["endCode"], tooltip_end_index, accession)
                elif data["dataType"] == "KnotProt":
                    tool_tip_content = "Type: Knot annotations<br>Label: {}<br>{}: {}{} - {}{}<br>Observed in 1 entry, eg. <a target=\"_blank\" href=\"https://www.ebi.ac.uk/pdbe/entry/pdb/{}\">{}</a><br>Source : <a target=\"_blank\" href=\"https://knotprot.cent.uw.edu.pl/chains/{}/{}/chain.xyz.txt\">KnotProt</a><br>Confidence level: {}".format(
                        residue["additionalData"]["groupLabel"], tooltip_range_label, residue["startCode"], tooltip_start_index, residue["endCode"], tooltip_end_index,
                        residue["pdbEntries"][0]["pdbId"], residue["pdbEntries"][0]["pdbId"], residue["pdbEntries"][0]["pdbId"],
                        residue["pdbEntries"][0]["chainIds"][0], residue["additionalData"]["confidenceClassification"], accession)
                elif data["dataType"] == "WEBnma":
                    tool_tip_content = "Type: Flexibility prediction<br>Label: {}<br>UniProt residue: {}{}<br>Observed in 1 entry, eg. <a target=\"_blank\" href=\"https://www.ebi.ac.uk/pdbe/entry/pdb/{}\">{}</a><br>Source: <a target=\"_blank\" href=\"http://apps.cbu.uib.no/webnma3\">WEBnma</a><br>Raw score: {}".format(
                        residue["additionalData"]["groupLabel"], residue["startCode"], tooltip_start_index, residue["pdbEntries"][0]["pdbId"],
                        residue["pdbEntries"][0]["pdbId"], residue["additionalData"]["rawScore"], accession)
                elif data["dataType"] == "KinCore":
                    chain_identifier = residue["pdbEntries"][0]["chainIds"][0]
                    pdb_id = residue["pdbEntries"][0]["pdbId"]
                    tool_tip_content = (
                        "Type: KinCore classification<br>"
                        f"""UniProt residue: {residue["startCode"]}{tooltip_start_index}<br>"""
                        f"""Label: {residue["additionalData"]["groupLabel"]}<br>"""
                        f"Chain identifier: {chain_identifier}<br>"
                        f"""Observed in {len(residue["pdbEntries"])} entry, eg. <a target=\"_blank\" href=\"https://www.ebi.ac.uk/pdbe/entry/pdb/{pdb_id}\">{pdb_id}</a><br>"""
                        f"Source: <a target=\"_blank\" href=\"http://dunbrack.fccc.edu/kincore/PDB/{pdb_id}\">KinCore</a><br>"
                        f"""Confidence level: {residue["additionalData"]["confidenceClassification"]}<br>"""
                    )
                elif data["dataType"] == "dynamine":
                    tool_tip_content = "Type: Flexibility prediction<br>Label: {}<br>UniProt residue: {}{}<br>Observed in 1 entry, eg. <a target=\"_blank\" href=\"https://www.ebi.ac.uk/pdbe/entry/pdb/{}\">{}</a><br>Source: <a target=\"_blank\" href=\"http://dynamine.ibsquare.be\">Dynamine</a><br>Raw score: {}".format(
                        residue["additionalData"]["groupLabel"], residue["startCode"], tooltip_start_index, residue["pdbEntries"][0]["pdbId"],
                        residue["pdbEntries"][0]["pdbId"], residue["additionalData"]["rawScore"], accession)
                elif data["dataType"] == "efoldmine":
                    tool_tip_content = "Type: Early folding residue prediction<br>Label: {}<br>UniProt residue: {}{}<br>Observed in 1 entry, eg. <a target=\"_blank\" href=\"https://www.ebi.ac.uk/pdbe/entry/pdb/{}\">{}</a><br>Source: <a target=\"_blank\" href=\"http://dynamine.ibsquare.be\">Dynamine</a><br>Raw score: {}".format(
                        residue["additionalData"]["groupLabel"], residue["startCode"], tooltip_start_index, residue["pdbEntries"][0]["pdbId"],
                        residue["pdbEntries"][0]["pdbId"], residue["additionalData"]["rawScore"], accession)
                    
                fragment = {
                    "start": start_index,
                    "end": end_index,
                    "pdb_start": residue["pdbStartIndex"],
                    "pdb_end": residue["pdbEndIndex"],
                    "tooltipContent": tool_tip_content
                }
                
                # assign color to residue if present
                if residue.get("color"):
                    fragment.update({"color": residue["color"]})

                master_dict[dict_key]["locations"][0]["fragments"].append(fragment)

        for (data_type, sec_structure_name), values in master_dict.items():
            values["locations"][0]["fragments"] = sorted(values["locations"][0]["fragments"], key=lambda x: x["start"])

            # IDP (MobiDB) need to be in separate track
            # WEBnma to be added to IDP track
            # dynamine to be added to IDP track
            if data_type == "MobiDB":
                idp_fragments.append(values)
            elif data_type == "KnotProt":
                topology_fragments.append(values)
            elif data_type == "WEBnma":
                idp_fragments.append(values)
            elif data_type == "KinCore":
                topology_fragments.append(values)
            elif data_type == "dynamine":
                idp_fragments.append(values)
            elif data_type == "efoldmine":
                efold_fragments.append(values)

        # PDBE-2673: add legends only if exists
        # for IDP, add legends and data to final list
        if idp_present or webnma_present or dynamine_present:
            specific_response[accession]["tracks"].append({
                "label": "Flexibility predictions",
                "labelType": "text",
                "data": idp_fragments,
                "labelColor": "rgb(128,128,128)"
            })

        if knotprot_present or kincore_present:
            specific_response[accession]["tracks"].append({
                "label": "Topology annotations",
                "labelType": "text",
                "data": topology_fragments,
            })
        
        if efold_present:
            specific_response[accession]["tracks"].append({
                "label": "Early folding residue predictions",
                "labelType": "text",
                "data": efold_fragments,
                "labelColor": "rgb(128,128,128)"
            })
    
        if idp_present:
            specific_response[accession]["legends"]["data"]["Flexibility predictions"].append({
                "text": "MobiDB",
                "color": common_color_codes["MobiDB"]
            })

        if knotprot_present:
            specific_response[accession]["legends"]["data"]["Flexibility predictions"].append({
                "text": "KnotProt",
                "color": common_color_codes["KnotProt"]
            })
            
        if kincore_present:
            specific_response[accession]["legends"]["data"]["Flexibility predictions"].append({
                "text": "KinCore",
                "color": common_color_codes["KinCore"]
            })

        if webnma_present:
            specific_response[accession]["legends"]["data"]["Flexibility predictions"].append({
                "text": "WEBnma",
                "color": common_color_codes["WEBnma"]
            })

        if dynamine_present:
            specific_response[accession]["legends"]["data"]["Flexibility predictions"].append({
                "text": "DynaMine",
                "color": common_color_codes["dynamine"]
            })
        
        if efold_present:
            specific_response[accession]["legends"]["data"]["Early folding residue predictions"].append({
                "text": "EFoldMine",
                "color": common_color_codes["dynamine"]
            })

    return specific_response


def convert_variation(generic_response):
    """
        Summary:
            Converts generic variation response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic variation response
        Returns:
            A protvista specific variation response object 
    """

    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")
    
    specific_response = {}

    for accession in generic_response.keys():
        accession_data = generic_response[accession]
        sequence = accession_data["sequence"]
        specific_response["sequence"] = accession_data["sequence"]
        specific_response["variants"] = []

        for feature in accession_data["features"]:
            if feature.get("clinicalSignificances") is None:
                feature["clinicalSignificances"] = None

            variant_fragment = {
                "accession": feature["genomicLocation"],
                "association": [] if feature.get("association") is None else feature["association"],
                "clinicalSignificances": feature["clinicalSignificances"],
                "color": get_variation_color(feature),
                "end": feature["end"],
                "polyphenScore": feature["polyphenScore"],
                "siftScore": feature["siftScore"],
                "sourceType": feature["sourceType"],
                "start": feature["begin"],
                "tooltipContent": format_variation_tooltip(feature),
                "variant": feature["alternativeSequence"],
                "xrefNames": [] if feature.get("xrefs") is None else get_source_type(feature["xrefs"], feature["sourceType"])
            }

            variant_fragment["keywords"] = get_variation_filter_keywords(variant_fragment)
            specific_response["variants"].append(variant_fragment)
        
    return specific_response

def convert_processed_protein(generic_response):
    """
        Summary:
            Converts generic processed proteins response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic processed proteins response
        Returns:
            A protvista specific processed proteins response object 
    """
    
    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")

    specific_response = {}
    
    for accession in generic_response.keys():
        accession_data = generic_response[accession]

        specific_response[accession] = {
            "largeLabels": True,
            "sequence": accession_data["sequence"],
            "length": accession_data["length"],
            "tracks": [{
                "labelType": "text",
                "label": "Processed proteins",
                "data": []
            }]
        }

        for data in accession_data["data"]:
            fragment = {
                "accession": data["name"],
                "labelType": "text",
                "label": data["name"],
                "labelTooltip": data["name"],
                "color": color_protvista_processed_proteins,
                "type": "UniProt range",
                "locations": [
                    {
                        "fragments": [{
                        "start": data["unp_start"],
                        "end": data["unp_end"],
                        "tooltipContent": "Type: Processed protein<br>Name: {}<br>Range: {}{} - {}{}<br>View protein page: <a target=\"_blank\" href=\"https://pdbe-kb.org/proteins/{}\">{}</a>".format(
                            data["name"], data["unp_start_code"], data["unp_start"], data["unp_end_code"], data["unp_end"], data["id"], data["id"])
                        }]
                    }
                ]
            }

            specific_response[accession]["tracks"][0]["data"].append(fragment)

    return specific_response


def convert_secondary_structures_variation(generic_response):
    """
        Summary:
            Converts generic secondary structures variation response to protvista specific response
        Args:
            generic_response (obj) - An object representation of generic secondary structures variation response
        Returns:
            A protvista specific secondary structures variation response object 
    """

    if type(generic_response) != dict or generic_response == {}:
        raise Exception("Invalid input")
    
    specific_response = {}
    master_dict = {}
    residue_dict = {}

    for accession in generic_response.keys():
        accession_data = generic_response[accession]

        specific_response[accession] = {
            "largeLabels": True,
            "sequence": accession_data["sequence"],
            "length": accession_data["length"],
            "tracks": [],
            "legends": {
                "alignment": "right",
                "data": {
                    "Secondary structure variation": []
                }
            }
        }
        for item in accession_data["data"]:
            residue_id = item["residueId"]
            sec_structure = item["secondaryStructure"]
            
            if not residue_dict.get(residue_id):
                residue_dict[residue_id] = {}
            residue_dict[residue_id].update({
                sec_structure: item["occurence"],
                # "count": len(item["pdbs"]),
            })
            residue_fragment = {
                "start": item["residueId"],
                "end": item["residueId"],
            }
            if not master_dict.get(sec_structure):
                label_tooltip = f"Residues forming {sec_structure.title()}"

                if sec_structure == "loop":
                    label_tooltip = "Residues not forming Helix or Strand"

                master_dict[sec_structure] = {
                    "accession": sec_structure,
                    "type": "UniProt range",
                    "label": sec_structure.title(),
                    "labelTooltip":label_tooltip,
                    "color": "",
                    "labelType": "text",
                    "locations": [{"fragments": []}],
                }
            master_dict[sec_structure]["locations"][0]["fragments"].append(residue_fragment)

        # prepare tooltip and color for each residue
        for key in master_dict.keys():
            for res_fragment in master_dict[key]["locations"][0]["fragments"]:
                residue_id = res_fragment["start"]
                stats = residue_dict[residue_id]
                tooltip = ""
                non_loop_occurence = 0
                
                # always make sure loop is last item
                for struct_type in ["helix", "strand", "loop"]:
                    occurence = stats.get(struct_type) or 0

                    # sum all non loop occurence
                    non_loop_occurence += occurence if struct_type != "loop" else 0

                    # add unobserved occurence to loop occurence
                    if struct_type == "loop":
                        occurence = (100 - non_loop_occurence)
                        stats.update({"loop": occurence})

                    tooltip += f"<font style='color:{color_codes[struct_type].__str__()}'>{struct_type.title()}:&nbsp;&nbsp;&nbsp;{round(occurence, 1)}%</font><br>"

                # tooltip += f"(Calculated from {stats.get('count')} structures)"
                res_fragment["tooltipContent"] = tooltip

                occurence = stats.get(key) or 0

                res_fragment["color"] = scale_color(occurence/100, one=color_codes[key]).__str__()


        specific_response[accession]["tracks"].append({
            "label": "Secondary structure variation",
            "labelType": "text",
            "data": [master_dict.get(x) for x in sec_structure_display_order if master_dict.get(x)],
        })

        # add legend for each secondary structure
        n_scale = 4
        for key in sec_structure_display_order:
            specific_response[accession]["legends"]["data"]["Secondary structure variation"].append({
                "color": [str(scale_color(i / n_scale, one=color_codes[key])) for i in range(1, n_scale+1)],
                "text": key.title(),
            })

    return specific_response