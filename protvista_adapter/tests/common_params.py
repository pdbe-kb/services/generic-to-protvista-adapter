
basic_structure = {
    "UNP": {
        "sequence": "MGQPGNGSA",
        "length": 100,
        "dataType": "type",
        "data": []
    }
}

basic_expected_keys = ["largeLabels", "sequence", "length", "tracks", "legends"]
