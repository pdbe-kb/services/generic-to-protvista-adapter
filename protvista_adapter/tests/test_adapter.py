import pytest
from protvista_adapter.adapter import convert_to_protvista
from protvista_adapter.tests.common_params import basic_structure, basic_expected_keys

def test_protein_page_interface_residues_invalid_input():

    with pytest.raises(Exception):
        convert_to_protvista("protein", "interface", {})

def test_protein_page_ligand_residues_invalid_input():

    with pytest.raises(Exception):
        convert_to_protvista("protein", "ligandsites", {})

def test_protein_page_annotations_invalid_input():

    with pytest.raises(Exception):
        convert_to_protvista("protein", "annotations", {})

def test_protein_page_domains_invalid_input():

    with pytest.raises(Exception):
        convert_to_protvista("protein", "domains", {})

def test_protein_page_unipdb_invalid_input():

    with pytest.raises(Exception):
        convert_to_protvista("protein", "unipdb", {})

def test_protein_page_secondary_structures_invalid_input():

    with pytest.raises(Exception):
        convert_to_protvista("protein", "secstructures", {})

def test_protein_page_interface_residues_basic_structure():

    output = convert_to_protvista("protein", "interface", basic_structure)
    assert output != {}

    for key in output.keys():
        for inner_key in output[key].keys():
            assert inner_key in basic_expected_keys

def test_protein_page_ligand_residues_basic_structure():

    output = convert_to_protvista("protein", "ligandsites", basic_structure)
    assert output != {}

    for key in output.keys():
        for inner_key in output[key].keys():
            assert inner_key in basic_expected_keys

def test_protein_page_annotations_basic_structure():

    output = convert_to_protvista("protein", "annotations", basic_structure)
    assert output != {}

    for key in output.keys():
        for inner_key in output[key].keys():
            assert inner_key in basic_expected_keys


def test_protein_page_domains_basic_structure():

    output = convert_to_protvista("protein", "domains", basic_structure)
    assert output != {}

    for key in output.keys():
        for inner_key in output[key].keys():
            assert inner_key in basic_expected_keys

def test_protein_page_unipdb_basic_structure():

    output = convert_to_protvista("protein", "unipdb", basic_structure)
    assert output != {}

    for key in output.keys():
        for inner_key in output[key].keys():
            assert inner_key in basic_expected_keys

def test_protein_page_secondary_structures_basic_structure():

    output = convert_to_protvista("protein", "secstructures", basic_structure)
    assert output != {}

    for key in output.keys():
        for inner_key in output[key].keys():
            assert inner_key in basic_expected_keys
            
            