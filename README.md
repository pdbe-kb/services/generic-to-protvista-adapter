# PDBe Graph API adapter for ProtVista
This is an adapter to convert generic graph API responses to ProtVista specific format.

## Installation
Below are the list of softwares/tools for the utilities to properly run in the environment.
```
Python 3.6+
```

Once the dependencies are available, install the package using pip.
```
pip3 install git+https://gitlab.ebi.ac.uk/pdbe-kb/services/generic-to-protvista-adapter.git
```
