import setuptools

setuptools.setup(
    name="protvista_adapter",
    version="1.0.12",
    url="",
    author="Sreenath Sasidharan Nair",
    author_email="sreenath@ebi.ac.uk",
    description="A set of utilities for converting generic PDBe Graph API response to protvista specific response.",
    long_description=open('README.md').read(),
    packages=setuptools.find_packages(),
    install_requires=["prodict"],
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
    ]
)
